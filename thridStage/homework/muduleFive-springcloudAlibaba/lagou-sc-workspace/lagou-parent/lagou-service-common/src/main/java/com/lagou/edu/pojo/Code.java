package com.lagou.edu.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/9 11:25
 **/
@Data
@Entity
@Table(name = "lagou_auth_code")
public class Code {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 主键
    private String email;
    private String code;
    private String createtime;
    private String expiretime;

}
