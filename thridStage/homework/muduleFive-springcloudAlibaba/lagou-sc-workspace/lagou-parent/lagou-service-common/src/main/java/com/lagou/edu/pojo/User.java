package com.lagou.edu.pojo;

import lombok.Data;

import javax.persistence.*;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/9 19:10
 **/
@Data
@Entity
@Table(name = "lagou_user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id; // 主键
    private String email;
    private String password;
}
