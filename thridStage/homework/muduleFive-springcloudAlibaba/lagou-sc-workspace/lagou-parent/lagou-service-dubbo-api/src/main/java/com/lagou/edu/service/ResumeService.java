package com.lagou.edu.service;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/23 22:29
 **/
public interface ResumeService {


    Integer findDefaultResumeByUserId(Long userId);

}
