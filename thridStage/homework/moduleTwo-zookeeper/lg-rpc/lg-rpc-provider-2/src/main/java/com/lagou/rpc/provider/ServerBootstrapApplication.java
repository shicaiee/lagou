package com.lagou.rpc.provider;

import com.lagou.rpc.provider.server.RpcServer;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/10 22:06
 **/
@SpringBootApplication
public class ServerBootstrapApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServerBootstrapApplication.class,args);
    }

    @Autowired
    RpcServer rpcServer;

    @Override
    public void run(String... args) throws Exception {
        new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                rpcServer.startServer("127.0.0.1",8899);
            }
        }).start();

    }
}
