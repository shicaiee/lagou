package com.lagou.edu.service;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.Message;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/7 23:46
 **/
@EnableBinding(value = {Sink.class})
public class MessageConsumerService {


    @StreamListener(Sink.INPUT)
    public void accept(Message<String> message){
        System.out.println("接收到消息：" + message);
    }


}
