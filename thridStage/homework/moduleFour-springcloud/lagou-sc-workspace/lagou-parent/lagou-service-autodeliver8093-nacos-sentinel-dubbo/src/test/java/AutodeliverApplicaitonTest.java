import com.lagou.edu.AutodeliverApplication8093;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@SpringBootTest(classes = {AutodeliverApplication8093.class})
@RunWith(SpringJUnit4ClassRunner.class)
public class AutodeliverApplicaitonTest {

    @Autowired
    private DiscoveryClient discoveryClient;

    @Test
    public void testMetaData(){
        List<ServiceInstance> instances = discoveryClient.getInstances("lagou-service-resume");
        // 2.选择一个
        ServiceInstance serviceInstance = instances.get(0);
        // 3.元数据信息 获取host port
        String host = serviceInstance.getHost();
        int port = serviceInstance.getPort();
    }


}
