package com.lagou.edu.controller;

import com.lagou.edu.service.ResumeService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/autodeliver")
public class AutodeliverController {

    @Reference
    private ResumeService resumeService;


    @GetMapping("/checkState/{userId}")
    public Integer findResumeOpenState(@PathVariable Long userId) throws InterruptedException {
        return resumeService.findDefaultResumeByUserId(userId);
    }


}
