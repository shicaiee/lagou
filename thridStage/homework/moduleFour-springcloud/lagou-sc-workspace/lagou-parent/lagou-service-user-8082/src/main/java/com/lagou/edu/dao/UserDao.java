package com.lagou.edu.dao;

import com.lagou.edu.pojo.Code;
import com.lagou.edu.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDao extends JpaRepository<User,Long> {


}
