package com.lagou.edu.controller;

import com.lagou.edu.componet.ConfigBean;
import com.lagou.edu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/config")
public class ConfigController {

    @Value("${server.port}")
    private int port;

    @Autowired
    private ConfigBean configBean;

    @Autowired
    private ResumeService resumeService;

    @GetMapping("/viewConfig")
    private String viewConfig() throws InterruptedException {
        return configBean.getConfig();
    }


}
