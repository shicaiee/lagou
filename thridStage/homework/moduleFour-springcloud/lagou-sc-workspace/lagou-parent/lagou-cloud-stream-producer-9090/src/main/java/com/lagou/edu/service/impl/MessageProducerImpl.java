package com.lagou.edu.service.impl;

import com.lagou.edu.service.IMessageProducer;
import com.netflix.discovery.converters.Auto;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;


/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/7 23:28
 **/
@EnableBinding(Source.class)
public class MessageProducerImpl implements IMessageProducer {

    @Autowired
    private Source source;

    @Override
    public void send(String content) {
        System.out.println("发送消息："+content);
        source.output().send(MessageBuilder.withPayload(content).build());
    }
}
