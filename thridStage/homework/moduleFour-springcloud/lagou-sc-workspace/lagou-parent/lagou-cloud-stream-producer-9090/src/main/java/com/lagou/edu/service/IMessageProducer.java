package com.lagou.edu.service;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/7 23:27
 **/
public interface IMessageProducer {

    public void send(String content);

}
