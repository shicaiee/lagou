package com.lagou.edu.controller;

import com.lagou.edu.pojo.Code;
import com.lagou.edu.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/9 10:04
 **/
@RestController
@RequestMapping("/code")
public class CodeController {

    @Autowired
    JavaMailSender jsm;

    @Autowired
    private CodeService codeService;

    @Value("${spring.mail.username}")
    private String username;
    /**
     *  ⽣成验证码并发送到对应邮箱，成功true，失败false
     */
    @GetMapping("/create/{email}")
    public boolean create(@PathVariable String email){
        try{
            int code = (int)(Math.random() * 9 + 1) * 100000;
            //建立邮箱消息
            SimpleMailMessage message = new SimpleMailMessage();
            //发送者
            message.setFrom(username);
            //接收者
            message.setTo(email);
            //发送标题
            message.setSubject("验证码");
            //发送内容
            message.setText(String.valueOf(code));
            jsm.send(message);
            // 验证码存到数据库中
            Code code1 = new Code();
            code1.setCode(String.valueOf(code));
            code1.setEmail(email);
            codeService.saveCode(code1);
            return true;
        }catch (Exception e){
            return false;
        }

    }

    /**
     *  校验验证码是否正确，0正确1错误2超时
     */
    @GetMapping("/validate/{email}/{password}/{code}")
    public int validate(@PathVariable String email,@PathVariable String password,@PathVariable String code){

        return 0;
    }

}
