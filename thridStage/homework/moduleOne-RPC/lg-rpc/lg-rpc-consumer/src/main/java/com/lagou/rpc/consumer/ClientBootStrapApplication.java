package com.lagou.rpc.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/12 22:45
 **/
@SpringBootApplication
public class ClientBootStrapApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientBootStrapApplication.class,args);
    }


}
