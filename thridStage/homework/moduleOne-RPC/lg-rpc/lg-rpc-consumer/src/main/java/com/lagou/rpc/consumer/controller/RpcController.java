package com.lagou.rpc.consumer.controller;

import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import com.lagou.rpc.pojo.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/12 22:47
 **/
@RestController
public class RpcController {

    private static AtomicInteger i = new AtomicInteger(1);

    private static final int[] ips = {8899,8898};

    @RequestMapping("/getData")
    public String getData(){
        int index = i.get() % 2;
        int port = ips[index];
        // 轮询访问 两个服务器
        IUserService proxy = (IUserService) new RpcClientProxy(port).createProxy(IUserService.class);
        i.incrementAndGet();
        return proxy.getById(1).toString();
    }

}