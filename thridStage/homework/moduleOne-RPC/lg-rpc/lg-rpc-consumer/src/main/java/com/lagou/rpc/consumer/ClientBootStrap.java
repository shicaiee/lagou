package com.lagou.rpc.consumer;

import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import com.lagou.rpc.pojo.User;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/11 10:45
 **/
public class ClientBootStrap {

    public static void main(String[] args) {
        IUserService proxy = (IUserService) new RpcClientProxy(8899).createProxy(IUserService.class);
        User user = proxy.getById(1);
        System.out.println(user);
    }


}
