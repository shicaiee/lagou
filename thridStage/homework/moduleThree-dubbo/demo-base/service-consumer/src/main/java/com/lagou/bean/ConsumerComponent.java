package com.lagou.bean;

import com.lagou.service.HelloService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/20 23:12
 **/
@Component
public class ConsumerComponent {

    @Reference //(loadbalance = "onlyFirst")
    private HelloService helloService;

    public String sayHello1(String name){
        return  helloService.sayHello1(name);
    }

    public String sayHello2(String name){
        return  helloService.sayHello2(name);
    }

    public String sayHello3(String name){
        return  helloService.sayHello3(name);
    }





}
