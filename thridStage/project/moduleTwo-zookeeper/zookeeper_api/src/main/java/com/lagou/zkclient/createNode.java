package com.lagou.zkclient;

import org.I0Itec.zkclient.ZkClient;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/16 16:38
 **/
public class createNode {


    public static void main(String[] args) {
        ZkClient zkClient = new ZkClient("127.0.0.1:2181");
        System.out.println("会话创建成功");

        zkClient.createPersistent("/lg-zkclient/c1", true);
        System.out.println("节点创建成功");
    }



}
