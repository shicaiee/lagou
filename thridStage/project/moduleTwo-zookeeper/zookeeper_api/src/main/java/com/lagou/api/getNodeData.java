package com.lagou.api;

import org.apache.zookeeper.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/15 23:14
 **/
public class getNodeData implements Watcher {


    private static ZooKeeper zooKeeper;

    /**
     * 建立会话
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        /**
         客户端可以通过创建⼀个zk实例来连接zk服务器
         new Zookeeper(connectString,sesssionTimeOut,Wather)
         connectString: 连接地址：IP：端⼝
         sesssionTimeOut：会话超时时间：单位毫秒
         Wather：监听器(当特定事件触发监听时，zk会通过watcher通知到客户端)
         */
        zooKeeper = new ZooKeeper("127.0.0.1:2181", 5000, new getNodeData());
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * 回调方法：处理来自服务器端的watcher事件通知
     */
    @Override
    public void process(WatchedEvent watchedEvent) {
        // 收到子节点修改通知
        if(watchedEvent.getType() == Event.EventType.NodeChildrenChanged){
            List<String> children = null;
            try {
                children = zooKeeper.getChildren("/lg-persistent", true);
            } catch (KeeperException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(children);
        }



        // SyncConnected
        if(watchedEvent.getState() == Event.KeeperState.SyncConnected){
            try {
                // 获取节点内容
                getNode();

                // 获取子节点
                getNodeChildren();
            } catch (KeeperException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }
    }

    /**
     * path:路径
     * watcher:是否开启监听
     */
    private static void getNode() throws KeeperException, InterruptedException, UnsupportedEncodingException {
        byte[] data = zooKeeper.getData("/lg-persistent", false, null);
        System.out.println(new String(data, "utf-8"));
    }

    private static void getNodeChildren() throws KeeperException, InterruptedException {
        List<String> children = zooKeeper.getChildren("/lg-persistent", true);
        System.out.println(children);
    }

}
