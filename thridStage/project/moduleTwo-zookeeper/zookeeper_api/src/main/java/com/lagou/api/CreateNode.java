package com.lagou.api;

import org.apache.zookeeper.*;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/15 23:14
 **/
public class CreateNode implements Watcher {

    private static CountDownLatch countDownLatch= new CountDownLatch(1);

    private static ZooKeeper zooKeeper;

    /**
     * 建立会话
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        /**
         客户端可以通过创建⼀个zk实例来连接zk服务器
         new Zookeeper(connectString,sesssionTimeOut,Wather)
         connectString: 连接地址：IP：端⼝
         sesssionTimeOut：会话超时时间：单位毫秒
         Wather：监听器(当特定事件触发监听时，zk会通过watcher通知到客户端)
         */
        zooKeeper = new ZooKeeper("127.0.0.1:2181", 5000, new CreateNode());
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * 回调方法：处理来自服务器端的watcher事件通知
     */
    @Override
    public void process(WatchedEvent watchedEvent) {
        // SyncConnected
        if(watchedEvent.getState() == Event.KeeperState.SyncConnected){
            // 创建节点
            try {
                createNodeSync();
            } catch (KeeperException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * path: 路劲
     * data[]: 保存的数据
     * acl:权限信息
     * createMode:节点类型
     */
    private static void createNodeSync() throws KeeperException, InterruptedException {
        String persistent = zooKeeper.create("/lg-persistent", "这是一个持久节点".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

        String ephemeral = zooKeeper.create("/lg-ephemeral", "这是一个临时节点".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);

        String persistentSequential = zooKeeper.create("/lg-persistent", "这是一个持久顺序节点".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT_SEQUENTIAL);

        System.out.println(persistent);
        System.out.println(persistentSequential);
        System.out.println(ephemeral);
    }
}
