package com.lagou.zkclient;

import org.I0Itec.zkclient.IZkChildListener;
import org.I0Itec.zkclient.ZkClient;

import java.util.List;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/16 16:38
 **/
public class getNode {


    public static void main(String[] args) throws InterruptedException {
        ZkClient zkClient = new ZkClient("127.0.0.1:2181");
        System.out.println("会话创建成功");

        /**
         * 注册监听
         */
        zkClient.subscribeChildChanges("/lg-zkclient-get", new IZkChildListener() {
            @Override
            public void handleChildChange(String s, List<String> list) throws Exception {
                System.out.println(s+"节点发生了变化，变化后的子节点集合为："+list);
            }
        });

        zkClient.createPersistent("/lg-zkclient-get");
        Thread.sleep(1000);


        zkClient.createPersistent("/lg-zkclient-get/c1");
        Thread.sleep(1000);

    }



}
