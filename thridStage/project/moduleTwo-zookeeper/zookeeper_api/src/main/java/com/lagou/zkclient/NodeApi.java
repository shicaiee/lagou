package com.lagou.zkclient;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/16 16:38
 **/
public class NodeApi {


    public static void main(String[] args) throws InterruptedException {
        ZkClient zkClient = new ZkClient("127.0.0.1:2181");
        System.out.println("会话创建成功");

        // 判断节点是否存在
        String path = "/lg-zkclient-Ep";
        boolean exists = zkClient.exists(path);

        if(!exists){
            zkClient.createEphemeral(path,"123");
        }

        // 读取节点内容
        Object data = zkClient.readData(path);
        System.out.println(data);

        // 注册监听
        zkClient.subscribeDataChanges(path, new IZkDataListener() {
            /**
             * 当节点数据内容发生变化时，执行的回调方法
             * s: path
             * o: 变化后的节点内容
             */
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                System.out.println(s+"该节点内容被更新，更新内容："+o);
            }

            /**
             * 当节点当节点被删除时，执行的回调方法
             * s: path
             */
            @Override
            public void handleDataDeleted(String s) throws Exception {
                System.out.println("该节点被删除");
            }
        });

        // 更新节点内容
        zkClient.writeData(path,"456");
        Thread.sleep(1000);

        // 删除节点
        zkClient.delete(path);
        Thread.sleep(1000);
    }



}