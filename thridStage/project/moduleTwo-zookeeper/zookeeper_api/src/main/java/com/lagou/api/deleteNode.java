package com.lagou.api;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/15 23:14
 **/
public class deleteNode implements Watcher {


    private static ZooKeeper zooKeeper;

    /**
     * 建立会话
     */
    public static void main(String[] args) throws IOException, InterruptedException {
        /**
         客户端可以通过创建⼀个zk实例来连接zk服务器
         new Zookeeper(connectString,sesssionTimeOut,Wather)
         connectString: 连接地址：IP：端⼝
         sesssionTimeOut：会话超时时间：单位毫秒
         Wather：监听器(当特定事件触发监听时，zk会通过watcher通知到客户端)
         */
        zooKeeper = new ZooKeeper("127.0.0.1:2181", 5000, new deleteNode());
        Thread.sleep(Integer.MAX_VALUE);
    }

    /**
     * 回调方法：处理来自服务器端的watcher事件通知
     */
    @Override
    public void process(WatchedEvent watchedEvent) {
        // SyncConnected
        if(watchedEvent.getState() == Event.KeeperState.SyncConnected){
            //删除
            try {
                deleteNodeSync();
            } catch (KeeperException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * path: 路劲
     * data[]: 保存的数据
     * version: 版本
     */
    private static void deleteNodeSync() throws KeeperException, InterruptedException, UnsupportedEncodingException {
        System.out.println(new String(zooKeeper.getData("/lg-persistent/c1", false, null), "utf-8"));

        Stat exists = zooKeeper.exists("/lg-persistent/c1", false);
        if(exists != null){
            zooKeeper.delete("/lg-persistent/c1",-1);
        }
        System.out.println(new String(zooKeeper.getData("/lg-persistent/c1", false, null), "utf-8"));
    }
}
