package com.lagou.curatorclient;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/16 21:20
 **/
public class CreateSession {
    public static void main(String[] args) {
        // 创建会话
        // 不使用fluent编程风格
        RetryPolicy exponentialBackoffRetry = new ExponentialBackoffRetry(1000, 3);
        CuratorFramework client = CuratorFrameworkFactory.newClient("127.0.0.1:2181", exponentialBackoffRetry);
        client.start();
        System.out.println("会话被建立了");

        // 使用fluent编程风格
        CuratorFramework baseClient = CuratorFrameworkFactory.builder()
                .connectString("127.0.0.1:2181")
                .sessionTimeoutMs(50000)
                .connectionTimeoutMs(30000)
                .retryPolicy(exponentialBackoffRetry)
                .namespace("base")  // 独立的命名空间  /base  所有操作都在这个路径下
                .build();
        baseClient.start();
        System.out.println("会话2被创建了");
    }
}
