package com.lagou.curatorclient;

import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/16 21:20
 **/
public class getNode {
    public static void main(String[] args) throws Exception {
        // 创建会话
        RetryPolicy exponentialBackoffRetry = new ExponentialBackoffRetry(1000, 3);

        // 使用fluent编程风格
        CuratorFramework baseClient = CuratorFrameworkFactory.builder()
                .connectString("127.0.0.1:2181")
                .sessionTimeoutMs(50000)
                .connectionTimeoutMs(30000)
                .retryPolicy(exponentialBackoffRetry)
                .namespace("base")  // 独立的命名空间  /base  所有操作都在这个路径下
                .build();
        baseClient.start();

        // 创建节点
        String path = "/lg-curator/c1";
        String s = baseClient.create().creatingParentsIfNeeded()
                .withMode(CreateMode.PERSISTENT).forPath(path, "init".getBytes());
        System.out.println("节点递归创建成功："+s);

        // 获取节点数据内容以及状态信息
        byte[] bytes = baseClient.getData().forPath(path);
        System.out.println(new String(bytes));

        Stat stat = new Stat();
        baseClient.getData().storingStatIn(stat).forPath(path);
        System.out.println(stat);
    }
}
