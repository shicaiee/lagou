package com.lagou.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import io.netty.util.CharsetUtil;

import java.util.List;

/**
 * @Desc    消息解码器
 * @Author Matures
 * @CreateTime 2021/4/7 22:55
 **/
public class MessageDecoder extends MessageToMessageDecoder {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, Object o, List list) throws Exception {
        System.out.println(" 正在进行消息解码。。。");
        ByteBuf byteBuf = (ByteBuf) o;
        // 传递到下一个handler
        list.add(byteBuf.toString(CharsetUtil.UTF_8));
    }
}
