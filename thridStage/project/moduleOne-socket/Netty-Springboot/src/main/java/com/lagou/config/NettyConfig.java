package com.lagou.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/8 16:28
 **/
@Component
@ConfigurationProperties(prefix = "netty")
@Data
public class NettyConfig {

    // netty监听的端口
    private int port;

    // websocket访问路劲
    private String path;
}
