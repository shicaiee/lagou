package com.lagou.netty;

import com.lagou.config.NettyConfig;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/8 16:41
 **/
@Component
public class WebSocketChannelInit extends ChannelInitializer {

    @Autowired
    NettyConfig nettyConfig;

    @Autowired
    WebSocketHandler webSocketHandler;


    @Override
    protected void initChannel(Channel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();
        // 对http协议的支持
        pipeline.addLast(new HttpServerCodec());

        // 对大数据流的支持
        pipeline.addLast(new ChunkedWriteHandler());

        // post请求分三部分  request line / request header  / message body
        // HttpObjectAggregator 将多个信息转化为单一的request 或者response
        pipeline.addLast(new HttpObjectAggregator(8000));

        // 将http协议升级为ws协议，对websocket的支持
        pipeline.addLast(new WebSocketServerProtocolHandler(nettyConfig.getPath()));

        // 添加自定义处理handler
        pipeline.addLast(webSocketHandler);
    }
}
