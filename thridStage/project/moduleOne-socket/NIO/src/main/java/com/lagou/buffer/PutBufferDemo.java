package com.lagou.buffer;

import java.nio.ByteBuffer;

/**
 * @Desc 向缓冲区中添加数据
 * @Author Matures
 * @CreateTime 2021/4/6 11:17
 **/
public class PutBufferDemo {

    public static void main(String[] args) {
        ByteBuffer allocate = ByteBuffer.allocate(10);
        System.out.println(allocate.position());
        System.out.println(allocate.limit());
        System.out.println(allocate.capacity());
        System.out.println(allocate.remaining());

        System.out.println("-----------------------");

        // 修改当前索引所在位置
        // allocate.position(1);
        // 修改最多能操作到那个索引的位置
        //allocate.limit(9);
        //System.out.println(allocate.position());
        //System.out.println(allocate.limit());
        //System.out.println(allocate.capacity());
        //System.out.println(allocate.remaining());

        // 添加一个字节
        allocate.put((byte) 97);

        System.out.println(allocate.position());
        System.out.println(allocate.limit());
        System.out.println(allocate.capacity());
        System.out.println(allocate.remaining());


        System.out.println("-----------------------");
        // 添加一个数组
        allocate.put("abc".getBytes());

        System.out.println(allocate.position());
        System.out.println(allocate.limit());
        System.out.println(allocate.capacity());
        System.out.println(allocate.remaining());

        System.out.println("-----------------------");
        // 添加一个数组
        allocate.put("abcvbf".getBytes());

        System.out.println(allocate.position());
        System.out.println(allocate.limit());
        System.out.println(allocate.capacity());
        System.out.println(allocate.remaining());

        // 如果缓存区满了，可以调整position位置，就可以覆盖之前存入索引位置的值
        allocate.position(1);
        System.out.println("-----------------------");
        allocate.put("123456".getBytes());
        System.out.println(allocate.position());
        System.out.println(allocate.limit());
        System.out.println(allocate.capacity());
        System.out.println(allocate.remaining());
    }
}
