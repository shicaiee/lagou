package com.lagou.buffer;

import java.nio.ByteBuffer;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/6 10:59
 **/
public class CreateBufferDemo {

    public static void main(String[] args) {
        // 1.创建指定长度的缓冲区 ByteBuffer为例
        ByteBuffer allocate = ByteBuffer.allocate(5);
        for (int i = 0; i < 5; i++) {
            // 从缓冲区拿取数据
            System.out.println(allocate.get());
        }
        // 从缓冲区拿取数据 会报错
        // System.out.println(allocate.get());

        // 2.创建一个有内容的缓冲区
        ByteBuffer wrap = ByteBuffer.wrap("lagou".getBytes());
        for (int i = 0; i < 5; i++) {
            System.out.println(wrap.get());
        }
    }
}
