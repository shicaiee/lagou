package com.lagou.buffer;

import java.nio.ByteBuffer;

/**
 * @Desc    从缓存区中读取数据
 * @Author Matures
 * @CreateTime 2021/4/6 19:20
 **/
public class GetBufferDemo {
    public static void main(String[] args) {
        // 1.创建一个指定长度缓冲区
        ByteBuffer allocate = ByteBuffer.allocate(10);
        allocate.put("0123".getBytes());

        //byte b = allocate.get();
        //System.out.println(b);

        // 切换读模式
        allocate.flip();

        for (int i = 0; i < allocate.limit(); i++) {
            System.out.println(allocate.get());
        }
        // 读取指定索引字节
        System.out.println(allocate.get(1));

        allocate.rewind();
        byte[] bytes = new byte[4];
        allocate.get(bytes);
        System.out.println(new String(bytes));

        byte[] array = allocate.array();
        System.out.println(new String(array));

        // 切换写模式
        allocate.clear();
        allocate.put("abc".getBytes());
        System.out.println(new String(allocate.array()));
    }
}
