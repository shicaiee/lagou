package com.lagou.rmi.service;

import com.lagou.rmi.pojo.User;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/10 20:49
 **/
public interface IUserService extends Remote {

    User getByUserId(int id) throws RemoteException;

}
