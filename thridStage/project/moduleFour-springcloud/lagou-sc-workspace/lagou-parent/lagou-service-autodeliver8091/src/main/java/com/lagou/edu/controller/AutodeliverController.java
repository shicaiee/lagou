package com.lagou.edu.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.print.attribute.standard.MediaSize;
import java.util.List;

@RestController
@RequestMapping("/autodeliver")
public class AutodeliverController {

    @Autowired
    private RestTemplate restTemplate;

//    @GetMapping("/checkState/{userId}")
//    public Integer findResumeOpenState(@PathVariable Long userId){
//        // 调用远程服务  简历微服务接口 restTemplate
//        return restTemplate.getForObject("http://localhost:8080/resume/openstate/" + userId, Integer.class);
//    }

    @Autowired
    private DiscoveryClient discoveryClient;

//    /**
//     * 改造：服务注册到eureka之后
//     */
//    @GetMapping("/checkState/{userId}")
//    public Integer findResumeOpenState(@PathVariable Long userId){
//        // todo 从eureka server中获取服务的实例信息，以及接口信息
//        // 1.获取 lagou-service-resume服务的实例信息
//        List<ServiceInstance> instances = discoveryClient.getInstances("lagou-service-resume");
//        // 2.选择一个
//        ServiceInstance serviceInstance = instances.get(0);
//        // 3.元数据信息 获取host port
//        String host = serviceInstance.getHost();
//        int port = serviceInstance.getPort();
//        String url = "http://" + host + ":" + port + "/resume/openstate/"+ userId;
//        System.out.println(url);
//        return restTemplate.getForObject(url, Integer.class);
//    }


//    /**
//     * 改造：服务注册到eureka之后ribbion
//     */
//    @GetMapping("/checkState/{userId}")
//    public Integer findResumeOpenState(@PathVariable Long userId){
//        String url = "http://lagou-service-resume/resume/openstate/" +userId;
//        return restTemplate.getForObject(url, Integer.class);
//    }


    /**
     * 提供者模拟处理超时
     */
    @HystrixCommand(
            // 线程池标识
            threadPoolKey = "findResumeOpenState",
            // 线程池细节属性配置
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",value = "1"), // 线程数
                    @HystrixProperty(name = "maxQueueSize",value = "20") // 等待队列长度
            },
            commandProperties = {
                    // 每一个属性都是一个HystrixProperty
                    @HystrixProperty(name ="execution.isolation.thread.timeoutInMilliseconds",value="2000")
            }
    )
    @GetMapping("/checkState/{userId}")
    public Integer findResumeOpenState(@PathVariable Long userId){
        String url = "http://lagou-service-resume/resume/openstate/" +userId;
        return restTemplate.getForObject(url, Integer.class);
    }

    /**
     * 处理超时，熔断，返回错误信息
     * 提供者抛出异常
     *
     * 以上信息都会返回到消费者，不希望把异常返回上游
     */
    @HystrixCommand(
            threadPoolKey = "findResumeOpenStateFallback",
            // 线程池细节属性配置
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize",value = "2"), // 线程数
                    @HystrixProperty(name = "maxQueueSize",value = "20") // 等待队列长度
            },
            commandProperties = {
                    // 每一个属性都是一个HystrixProperty
                    // @HystrixProperty(name ="execution.isolation.thread.timeoutInMilliseconds",value="2000")
                    // 统计时间窗口定义
                    @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds",value = "8000"),
                    // 统计时间窗口内的最小请求数
                    @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold",value = "2"),
                    // 统计时间窗口内的错误数量百分比阈值
                    @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage",value = "50"),
                    // 自我修复时的活动窗口长度
                    @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds",value = "3000")
            },fallbackMethod = "myFallBack"
    )
    @GetMapping("/checkStateFallback/{userId}")
    public Integer findResumeOpenStateFallback(@PathVariable Long userId){
        String url = "http://lagou-service-resume/resume/openstate/" +userId;
        return restTemplate.getForObject(url, Integer.class);
    }

    // 定义回退方法，形参和返回值与原始方法保持一致
    public Integer myFallBack(Long userId){
        return -1;  // 兜底数据
    }

}
