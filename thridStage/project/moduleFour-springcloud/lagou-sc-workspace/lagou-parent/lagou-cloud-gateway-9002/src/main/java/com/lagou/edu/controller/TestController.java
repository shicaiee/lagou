package com.lagou.edu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/9 15:09
 **/
@RestController
@RequestMapping("/getWay")
public class TestController {

    @GetMapping("/hello")
    public String hello(){
        return "hello world!";
    }
}
