package com.lagou.edu.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/5 19:48
 **/

/**
 * 定义全局过滤器，会对所有路由生效
 */
@Slf4j
@Component
public class LimitFilter implements GlobalFilter, Ordered {

    private static Map<String,Integer> limitMap = new HashMap();

    /**
     * 五秒清空一次
     */
    @Scheduled(fixedDelay = 5*1000)
    public void refresh(){
        Iterator<Map.Entry<String, Integer>> iterator = limitMap.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, Integer> entry = iterator.next();
            entry.setValue(0);
        }
    }

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();
        String clientIp = request.getRemoteAddress().getHostString();
        Integer integer = limitMap.get(clientIp);
        if(integer!= null && integer > 5){
            // 五秒内大于10此
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            String data = "request be denied!";
            DataBuffer wrap = response.bufferFactory().wrap(data.getBytes());
            return response.writeWith(Mono.just(wrap));
        }
        limitMap.put(clientIp,integer==null?0:integer+1);
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
