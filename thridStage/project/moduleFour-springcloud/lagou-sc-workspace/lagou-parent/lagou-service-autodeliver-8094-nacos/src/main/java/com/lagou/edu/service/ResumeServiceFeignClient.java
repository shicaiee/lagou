package com.lagou.edu.service;

import com.lagou.edu.pojo.Resume;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

// 表明当前类是一个Feign客户端，value指定改客户端要请求的服务名称（注册中心的服务提供者的名称）
@FeignClient(value = "lagou-service-resume",fallback = ResumeFallback.class,path = "/resume")
//@RequestMapping("/resume")
public interface ResumeServiceFeignClient {

    // Feign 要做的事情就是，拼装url发起请求
    @GetMapping("/openstate/{userId}")
    public Integer findDefaultResumeState(@PathVariable Long userId) throws InterruptedException;

}
