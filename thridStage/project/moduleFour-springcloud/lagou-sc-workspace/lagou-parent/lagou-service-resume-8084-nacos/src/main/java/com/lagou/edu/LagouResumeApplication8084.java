package com.lagou.edu;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EntityScan("com.lagou.edu.pojo")
@EnableDiscoveryClient // 开启注册中心客户端 （通用型注解）
                        // 说明 从SpringCloud 的Edgware 开始 不加注解也行
public class LagouResumeApplication8084 {

    public static void main(String[] args) {
        SpringApplication.run(LagouResumeApplication8084.class,args);
    }
}
