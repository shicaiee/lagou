package com.lagou.edu.componet;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/6 10:29
 **/
@Component
@Data
@RefreshScope
public class ConfigBean {

    //@Value("${config.info}")
    private String config;



}
