package com.lagou.edu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/9 10:04
 **/
//@RestController
//@RequestMapping("/resume")
public class UserController {

    @GetMapping("/hello")
    public String hello(){
        return "hello world!";
    }

}
