package com.lagou.edu.controller;

import com.lagou.edu.pojo.Resume;
import com.lagou.edu.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/resume")
public class ResumeController {

    @Value("${server.port}")
    private int port;

    @Autowired
    private ResumeService resumeService;

    @GetMapping("/openstate/{userId}")
    private Integer findDefaultResumeState(@PathVariable Long userId) throws InterruptedException {
        Resume defaultResumeByUserId = resumeService.findDefaultResumeByUserId(userId);
        //return defaultResumeByUserId.getIsOpenResume();
        return port;
    }

    @GetMapping("/hello")
    public String hello(){
        return "hello world!";
    }


}
