package com.lagou.edu;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EntityScan("com.lagou.edu.pojo")
// @EnableEurekaClient // 开启eureka客户端
@EnableDiscoveryClient // 开启注册中心客户端 （通用型注解）
                        // 说明 从SpringCloud 的Edgware 开始 不加注解也行

public class LagouEmailApplication8083 {

    public static void main(String[] args) {
        SpringApplication.run(LagouEmailApplication8083.class,args);
    }
}
