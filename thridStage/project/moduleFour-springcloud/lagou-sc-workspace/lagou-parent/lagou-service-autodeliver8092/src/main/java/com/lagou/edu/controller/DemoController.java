package com.lagou.edu.controller;

import com.lagou.edu.service.ResumeServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo")
public class DemoController {



    /**
     * 提供者模拟处理超时
     */
    @GetMapping("/test")
    public String findResumeOpenState() throws InterruptedException {
        Object details = SecurityContextHolder.getContext().getAuthentication().getDetails();
        return "demo/test";
    }


}
