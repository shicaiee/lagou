package com.lagou.edu.controller;

import com.lagou.edu.service.ResumeServiceFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.persistence.criteria.CriteriaBuilder;
import javax.print.attribute.standard.MediaSize;
import java.util.List;

@RestController
@RequestMapping("/autodeliver")
public class AutodeliverController {


    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private ResumeServiceFeignClient client;

    /**
     * 提供者模拟处理超时
     */
    @GetMapping("/checkState/{userId}")
    public Integer findResumeOpenState(@PathVariable Long userId) throws InterruptedException {
        Thread.sleep(5000);
        return client.findDefaultResumeState(userId);
    }


}
