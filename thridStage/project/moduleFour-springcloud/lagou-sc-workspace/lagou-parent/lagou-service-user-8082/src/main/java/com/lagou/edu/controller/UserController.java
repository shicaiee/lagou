package com.lagou.edu.controller;

import com.lagou.edu.dao.CodeDao;
import com.lagou.edu.dao.TokenDao;
import com.lagou.edu.dao.UserDao;
import com.lagou.edu.pojo.Code;
import com.lagou.edu.pojo.Token;
import com.lagou.edu.pojo.User;
import com.lagou.edu.service.CodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/5/9 10:04
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private CodeService codeService;

    @Autowired
    private CodeDao codeDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private TokenDao tokenDao;

    /**
     * 此时只需要
     * 查询数据库中该邮箱地址对应的最近⼀次的验证码记录，校验验证码是否正确，是否超时，若有问题，
     * 准确提示给⽤户
     */
    @GetMapping("/register/{email}/{password}/{codeStr}")
    public int register(@PathVariable String email,@PathVariable String password,@PathVariable String codeStr){
        Code code = new Code();
        code.setEmail(email);
        Example<Code> of = Example.of(code);
        List<Code> list = codeDao.findAll(of);
        Code resulet = list.get(list.size()-1);
        // 判断提交的验证码与数据库中存储的验证码是否相符
        if(resulet.getCode().equals(codeStr)){
            // 存入user表中
            User user = new User();
            user.setEmail(email);
            Example<User> ofUser = Example.of(user);
            if (userDao.findOne(ofUser).isPresent()) {
                return 1;
            }
            user.setPassword(password);
            userDao.save(user);
            return 0;
        }
        return 1;
    }

    @GetMapping("/isRregistered/{email}")
    public Boolean isRregistered(@PathVariable String email){
        return true;
    }


    @GetMapping("/login/{email}/{password}")
    public String login(@PathVariable String email, @PathVariable String password,
                        HttpServletRequest request, HttpServletResponse response){
        User user = new User();
        user.setEmail(email);
        Example<User> of = Example.of(user);
        User user1 = userDao.findOne(of).get();
        if(user1.getPassword().equals(password)){
            // 登录成功,根据⽤户名和密码⽣成token，token存⼊数据库，并写⼊cookie中，登录成功返回邮箱地址
            String uuid = UUID.randomUUID().toString();
            Token token = new Token();
            token.setEmail(email);
            token.setToken(uuid);
            tokenDao.save(token);
            Cookie cookie = new Cookie("token", uuid);
            cookie.setMaxAge(30 * 24 * 60 * 60);
            cookie.setPath(request.getContextPath());
            response.addCookie(cookie);
            return uuid;
        }
        return "false";
    }


    @GetMapping("/info/{token}")
    public String info(@PathVariable String token, HttpServletResponse response){
        Token token1 = new Token();
        token1.setToken(token);
        Example<String> of = Example.of(token);
        Token token2 = tokenDao.findAll().get(0);
        response.addCookie(new Cookie("token",token));
        return token2.getEmail();
    }

}
