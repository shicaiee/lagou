package com.lagou.edu.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.client.JdbcClientDetailsService;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;

import javax.sql.DataSource;

/**
 * Oauth2 配置类
 */
@Configuration
@EnableAuthorizationServer  // 开启认证服务器功能
public class OauthServerConfiger extends AuthorizationServerConfigurerAdapter {


    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private DataSource dataSource;


    /**
     *  认证服务器最终是以api接口的方式对外提供服务（校验合法性并生成令牌，校验令牌等）
     *  那么，以api接⼝⽅式对外的话，就涉及到接⼝的访问权限，我们需要在这⾥进⾏必要的配置
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        super.configure(security);
        // 相当于打开endpoints 访问接口的开关，这样的话后期我们能够访问该接口
        security
                // 允许客户端表单验证
                .allowFormAuthenticationForClients()
                // 开启端口 /oauth/token_key 的访问权限
                .tokenKeyAccess("permitAll()")
                // 开启端口/oauth/check_token 的访问权限
                .checkTokenAccess("permitAll()");
    }

    /**
     *  客户端详情，比如client_id,secret,当前这个服务就如同QQ平台，拉勾网作为客户端需要QQ平台进行登录授权认证等，
     *  提前需要到QQ平台注册，QQ平台会给拉钩网颁发client_id等必要参数，表明客户端是谁
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        super.configure(clients);
        // 从内存中加载客户端详情
//        clients.inMemory()  // 客户端信息存储在什么地方，可以在内存，可以在数据库
//                .withClient("client_lagou")    // 添加一个client配置，指定其client_id
//                .secret("abcxyz")   //指定客户端密码、安全码
//                .resourceIds("autodeliver") // 指定客户端能访问的资源id清单
//                .authorizedGrantTypes("password","refresh_token")//认证类型、令牌颁发模式
//                .scopes("all");// 客户端权限范围

        // 从数据库中加载客户端详情
        clients.withClientDetails(creatJdbcClientDetailsService());
    }

    @Bean
    public JdbcClientDetailsService creatJdbcClientDetailsService(){
        JdbcClientDetailsService jdbc = new JdbcClientDetailsService(dataSource);
        return jdbc;
    }


    /**
     * 配置token令牌管理相关
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        super.configure(endpoints);
        endpoints.tokenStore(tokenStore())// 指定token的存储方式
                    .tokenServices(authorizationServerTokenServices()) // token服务的一个描述，token细节的描述
                    // 指定认证管理器
                    .authenticationManager(authenticationManager)
                    .allowedTokenEndpointRequestMethods(HttpMethod.GET,HttpMethod.POST);
    }

    /**
     * 该方法用于创建tokenStore对象
     * token以什么形式存储
     */
    public TokenStore tokenStore(){
//        return new InMemoryTokenStore();
        return new JwtTokenStore(jwtAccessTokenConverter());
    }

    private String sign_key = "lagou123";

    @Autowired
    private LagouAccessTokenConvertor lagouAccessTokenConvertor;

    /**
     * 返回JwtAccessTokenConverter令牌转换器（帮助我们生成jwt令牌的）
     * 在这里，我们可以把签名密钥传递进去给转换器对象
     */
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey(sign_key); // 签名密钥
        jwtAccessTokenConverter.setVerifier(new MacSigner(sign_key));// 验证时使用的密钥
        jwtAccessTokenConverter.setAccessTokenConverter(lagouAccessTokenConvertor);
        return jwtAccessTokenConverter;
    }


    /**
     * 该方法用于创建token服务对象
     * token有效期
     */
    public AuthorizationServerTokenServices authorizationServerTokenServices(){
        // 使用默认实现
        DefaultTokenServices defaultTokenServices = new DefaultTokenServices();
        defaultTokenServices.setSupportRefreshToken(true); // 是否开启令牌刷新
        defaultTokenServices.setTokenStore(tokenStore());

        // 针对jwt令牌添加
        defaultTokenServices.setTokenEnhancer(jwtAccessTokenConverter());

        //
        // 设置令牌有效时间
        defaultTokenServices.setAccessTokenValiditySeconds(200); // access_token 就是请求资源需要携带的令牌
        // 刷新令牌的有限时间
        defaultTokenServices.setRefreshTokenValiditySeconds(259200); //3天
        return defaultTokenServices;
    }




}
