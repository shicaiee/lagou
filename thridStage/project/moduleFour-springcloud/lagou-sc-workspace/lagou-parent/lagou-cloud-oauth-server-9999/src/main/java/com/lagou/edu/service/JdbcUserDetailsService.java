package com.lagou.edu.service;

import com.lagou.edu.dao.UserRepository;
import com.lagou.edu.pojo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class JdbcUserDetailsService implements UserDetailsService {


    @Autowired
    private UserRepository userRepository;

    /**
     * 根据username查询该用户所有的信息，封装成UserDetails类型的对象返回
     * 至于密码，框架会自动匹配
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = userRepository.findByUsername(username);
        User user = new User(users.getUsername(),users.getPassword(),new ArrayList<>());
        return user;
    }
}
