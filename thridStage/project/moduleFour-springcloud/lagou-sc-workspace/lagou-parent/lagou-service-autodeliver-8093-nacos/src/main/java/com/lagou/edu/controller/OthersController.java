package com.lagou.edu.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/others")
public class OthersController {



    /**
     * 提供者模拟处理超时
     */
    @GetMapping("/test")
    public String findResumeOpenState() throws InterruptedException {
        return "others/test";
    }


}
