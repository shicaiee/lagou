package com.lagou;

import com.lagou.service.HelloService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/21 22:11
 **/
public class ConsumerApplication {

    public static void main(String[] args) throws IOException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:dubbo-consumer.xml");
        context.start();
        HelloService service = context.getBean(HelloService.class);
        System.out.println(service.sayHello("liusong"));
        System.in.read();
    }

}
