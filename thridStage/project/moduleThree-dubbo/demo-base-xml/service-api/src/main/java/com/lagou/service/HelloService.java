package com.lagou.service;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/21 21:52
 **/
public interface HelloService {

    String sayHello(String name);

}
