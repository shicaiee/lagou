package com.lagou.service.impl;

import com.lagou.service.HelloService;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/21 21:54
 **/
public class HelloServiceImpl implements HelloService {
    @Override
    public String sayHello(String name) {
        System.out.println("接口被调用");
        return "hello"+name;
    }
}
