package com.lagou.service.impl;

import com.lagou.service.HelloService;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/22 23:09
 **/
public class DogHelloService implements HelloService {
    @Override
    public String sayHello(String name) {
        return "wang wang"+name;
    }
}
