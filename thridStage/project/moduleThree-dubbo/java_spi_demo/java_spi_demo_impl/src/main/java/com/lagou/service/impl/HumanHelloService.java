package com.lagou.service.impl;

import com.lagou.service.HelloService;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/22 23:09
 **/
public class HumanHelloService implements HelloService {
    @Override
    public String sayHello(String name) {
        return "你好 "+name;
    }
}
