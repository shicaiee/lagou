package com.lagou.test;

import com.lagou.service.HelloService;

import java.util.ServiceLoader;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/22 23:23
 **/
public class HelloTest {

    public static void main(String[] args) {
        final ServiceLoader<HelloService> helloServices = ServiceLoader.load(HelloService.class);
        for (HelloService helloService : helloServices) {
            System.out.println(helloService.getClass().getName()+":"+helloService.sayHello("刘松"));
        }
    }

}
