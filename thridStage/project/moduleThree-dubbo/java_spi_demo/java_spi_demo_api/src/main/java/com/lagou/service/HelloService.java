package com.lagou.service;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/22 23:07
 **/
public interface HelloService {
    String sayHello(String name);
}
