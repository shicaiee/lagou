package com;

import com.lagou.bean.ConsumerComponent;
import com.lagou.service.HelloService;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.apache.dubbo.rpc.RpcContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/20 23:18
 **/
public class XMLConsumerMain {

    public static void main(String[] args) throws InterruptedException, IOException, ExecutionException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("consumer.xml");
        HelloService bean = context.getBean(HelloService.class);
        while (true){
            System.in.read();
            System.out.println("result:"+bean.sayHello1("liusong"));

            // 获取异步调用结果
            Future<Object> future = RpcContext.getContext().getFuture();
            System.out.println(future.get());
        }
    }


}
