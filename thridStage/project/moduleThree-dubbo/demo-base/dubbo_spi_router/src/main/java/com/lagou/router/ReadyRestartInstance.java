package com.lagou.router;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.zookeeper.data.Stat;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/26 23:41
 **/
public class ReadyRestartInstance implements PathChildrenCacheListener {

    private static final String LISTEN_PATHS = "/lagou/";

    private final CuratorFramework zkClient;


    private Set<String> restartInstances = new HashSet<>();

    private ReadyRestartInstance(CuratorFramework client){
        this.zkClient = client;
    }

    public static ReadyRestartInstance create(){
        CuratorFramework client = ZookeeperClients.client();
        try {
            Stat stat = client.checkExists().forPath(LISTEN_PATHS);
            if(stat == null){
                client.create().creatingParentsIfNeeded().forPath(LISTEN_PATHS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ReadyRestartInstance instance = new ReadyRestartInstance(client);
        PathChildrenCache nodeCache = new PathChildrenCache(client, LISTEN_PATHS, false);
        nodeCache.getListenable().addListener(instance);
        try {
            nodeCache.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return instance;
    }


    /** 返回应用名  和  主机拼接后的字符串 */
    private  String   buildApplicationAndInstanceString(String  applicationName,String host){
        return  applicationName + "_" + host;
    }
    /** 增加重启实例的配置信息方法 */
    public void  addRestartingInstance(String applicationName,String host) throws  Exception{
        zkClient.create().creatingParentsIfNeeded().forPath(LISTEN_PATHS + "/" + buildApplicationAndInstanceString(applicationName,host));
    }
    /** 删除重启实例的配置信息方法 */
    public void removeRestartingInstance(String applicationName,String host) throws  Exception{
        zkClient.delete().forPath(LISTEN_PATHS + "/" + buildApplicationAndInstanceString(applicationName,host));
    }
    /** 判断节点信息 是否存在于 restartInstances */
    public  boolean  hasRestartingInstance(String applicationName,String host){
        return  restartInstances.contains(buildApplicationAndInstanceString(applicationName,host));
    }



    @Override
    public void childEvent(CuratorFramework curatorFramework, PathChildrenCacheEvent pathChildrenCacheEvent) throws Exception {
        List<String> children = zkClient.getChildren().forPath(LISTEN_PATHS);
        if(CollectionUtils.isEmpty(children)){
            restartInstances = Collections.emptySet();
        }else {
            restartInstances = new HashSet<>(children);
        }
    }
}
