package com.lagou.service;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/4/20 22:53
 **/
public interface HelloService {

    String sayHello1(String name);

    String sayHello2(String name);

    String sayHello3(String name);

}
