package com.lagou.service.impl;

import com.lagou.service.HelloService;
import org.apache.dubbo.common.URL;

public class HumanHelloService implements HelloService {
    public String sayHello() {
        return "你好！";
    }

    public String sayHelloUrl(URL url) {
        return "你好 url";
    }
}
