package com.lagou.service.impl;

import com.lagou.service.HelloService;
import org.apache.dubbo.common.URL;

public class DogHelloService implements HelloService {
    public String sayHello() {
        return "wang wang";
    }

    public String sayHelloUrl(URL url) {
        return "dog url";
    }
}
