package com.lagou;

import com.lagou.service.HelloService;
import org.apache.dubbo.common.extension.ExtensionLoader;

import java.util.Set;

public class DubboSpiMain {

    public static void main(String[] args) {
        // 方法一
        ExtensionLoader<HelloService> extensionLoader = ExtensionLoader.getExtensionLoader(HelloService.class);
        Set<HelloService> supportedExtensionInstances = extensionLoader.getSupportedExtensionInstances();
        for (HelloService supportedExtensionInstance : supportedExtensionInstances) {
            System.out.println(supportedExtensionInstance.sayHello());
        }

        // 方法二
        Set<String> supportedExtensions = extensionLoader.getSupportedExtensions();
        for (String supportedExtension : supportedExtensions) {
            System.out.println(extensionLoader.getExtension(supportedExtension).sayHello());
        }
    }

}
