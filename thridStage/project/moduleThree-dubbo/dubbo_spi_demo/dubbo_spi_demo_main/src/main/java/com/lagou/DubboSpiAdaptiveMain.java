package com.lagou;

import com.lagou.service.HelloService;
import org.apache.dubbo.common.URL;
import org.apache.dubbo.common.extension.ExtensionLoader;

import java.util.Set;

public class DubboSpiAdaptiveMain {

    public static void main(String[] args) {

        URL url = URL.valueOf("?hello.service=human");
        HelloService adaptiveExtension = ExtensionLoader.getExtensionLoader(HelloService.class).getAdaptiveExtension();
        System.out.println(adaptiveExtension.sayHelloUrl(url));

        HelloService instances = (HelloService) ExtensionLoader.getExtensionLoader(HelloService.class).getLoadedAdaptiveExtensionInstances();
        System.out.println(instances.sayHelloUrl(url));

    }

}
