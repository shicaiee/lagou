package com.lagou.filter;

import org.apache.dubbo.common.constants.CommonConstants;
import org.apache.dubbo.common.extension.Activate;
import org.apache.dubbo.rpc.*;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;


@Activate(group = {CommonConstants.PROVIDER,CommonConstants.CONSUMER})
public class TimeFilter implements Filter {

//    private static int maxSize = 1000;


    // 记录每个方法调用时间 <方法调用时间,耗时>
//    public static HashMap<Long,Long> sayHell01 = new LinkedHashMap<Long, Long>(){
//        @Override
//        protected boolean removeEldestEntry(Map.Entry<Long, Long> eldest) {
//            return size() > maxSize;
//        }
//    };
//
//    public static HashMap<Long,Long> sayHell02 = new LinkedHashMap<Long, Long>(){
//        @Override
//        protected boolean removeEldestEntry(Map.Entry<Long, Long> eldest) {
//            return size() > maxSize;
//        }
//    };
//
//    public static HashMap<Long,Long> sayHell03 = new LinkedHashMap<Long, Long>(){
//        @Override
//        protected boolean removeEldestEntry(Map.Entry<Long, Long> eldest) {
//            return size() > maxSize;
//        }
//    };

    // 记录每个方法调用时间 <方法调用时间,耗时>
    public static ArrayList<String> h1 = new ArrayList<String>();

    public static ArrayList<String> h2 = new ArrayList<String>();

    public static ArrayList<String> h3 = new ArrayList<String>();




    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        long time = System.currentTimeMillis();
        try {
            return invoker.invoke(invocation);
        } finally {
            long endTime = System.currentTimeMillis();
            String methodName = invocation.getMethodName();
            if ("sayHello1".equals(methodName)) {
                h1.add(endTime+":"+ (endTime - time));
            } else if ("sayHello2".equals(methodName)) {
                h2.add(endTime+":"+ (endTime - time));
            } else if ("sayHello3".equals(methodName)) {
                h3.add(endTime+":"+ (endTime - time));
            }
//            System.out.println("执行时间：" + ( endTime- time) +"毫秒");
        }
    }
}
