# MyBatis笔记

##一、自定义持久层框架
### 解决的问题

+ 数据库连接创建、释放频繁造成系统资源浪费，从⽽影响系统性能

+ Sql语句在代码中硬编码，造成代码不易维护，实际应⽤中sql变化的可能较⼤，sql变动需要改变
  java代码

+ 使⽤preparedStatement向占有位符号传参数存在硬编码，因为sql语句的where条件不⼀定，可能
  多也可能少，修改sql还要修改代码，系统不易维护

+ 对结果集解析存在硬编码(查询列名)，sql变化导致解析代码变化，系统不易维护，如果能将数据 库
  记录封装成pojo对象解析⽐较⽅便

### 如何解决

+ 使⽤数据库连接池初始化连接资源
+ 将sql语句抽取到xml配置⽂件中
+ 使⽤反射、内省等底层技术，⾃动将实体与表进⾏属性与字段的⾃动映射


## 二、MyBatis知识点

### MyBatis插件

#### 插件介绍

​	Mybati s作为⼀个应⽤⼴泛的优秀的ORM开源框架，这个框架具有强⼤的灵活性，在四⼤组件
(Executor、StatementHandler、ParameterHandler、ResultSetHandler)处提供了简单易⽤的插 件扩
展机制。Mybatis对持久层的操作就是借助于四⼤核⼼对象。MyBatis⽀持⽤插件对四⼤核⼼对象进 ⾏
拦截，对mybatis来说插件就是拦截器，⽤来增强核⼼对象的功能，增强功能本质上是借助于底层的 动
态代理实现的，换句话说，MyBatis中的四⼤对象都是代理对象

 #### 插件原理

在四大对象创建的时候

+ 每个创建出来的对象不是直接返回的，⽽是interceptorChain.pluginAll(parameterHandler)
+ 获取到所有的Interceptor (拦截器)(插件需要实现的接⼝)；调⽤ interceptor.plugin(target);返
  回 target 包装后的对象
+ 插件机制，我们可以使⽤插件为⽬标对象创建⼀个代理对象；AOP (⾯向切⾯)我们的插件可 以
  为四⼤对象创建出代理对象，代理对象就可以拦截到四⼤对象的每⼀个执⾏

## 三、架构原理以及源码分析

![](图片.png)

### 主要组件以及相互关系

| 构件               | 描述                                       |
| ---------------- | ---------------------------------------- |
| SqlSession       | 作为MyBatis⼯作的主要顶层API，表示和数据库交互的会话，完成必要数    |
| Executor         | MyBatis执⾏器，是MyBatis调度的核⼼，负责SQL语句的⽣成和查询缓  |
| StatementHandler | 封装了JDBC Statement操作，负责对JDBC statement的操作，如设置参 |
| ParameterHandler | 负责对⽤户传递的参数转换成JDBC Statement所需要的参数        |
| ResultSetHandler | 负责将JDBC返回的ResultSet结果集对象转换成List类型的集合     |
| TypeHandler      | 负责java数据类型和jdbc数据类型之间的映射和转换              |
| MappedStatement  | MappedStatement维护了⼀条＜select \| update \| delete \| insert＞节点 |
| SqlSource        | 负责根据⽤户传递的parameterObject，动态地⽣成SQL语句，将信息封 |
| BoundSql         | 表示动态⽣成的SQL语句以及相应的参数信息                    |

### 层次结构

![层次结构](层次结构.png)

### 源码剖析

#### 初始化

```java
// 1.我们最初调⽤的build
public SqlSessionFactory build (InputStream inputStream){
//调⽤了重载⽅法
return build(inputStream, null, null);
}
// 2.调⽤的重载⽅法
public SqlSessionFactory build (InputStream inputStream, String
environment,
Properties properties){
try {
// XMLConfigBuilder是专⻔解析mybatis的配置⽂件的类
XMLConfigBuilder parser = new XMLConfigBuilder(inputstream,
environment, properties);
//这⾥⼜调⽤了⼀个重载⽅法。parser.parse()的返回值是Configuration对象
return build(parser.parse());
} catch (Exception e) {
throw ExceptionFactory.wrapException("Error building
SqlSession.", e)
}
```

MyBatis在初始化的时候，会将MyBatis的配置信息全部加载到内存中，使⽤
org.apache.ibatis.session.Configuratio n 实例来维护

#### SqlSession

SqlSession是⼀个接⼝，它有两个实现类：DefaultSqlSession (默认)和
SqlSessionManager (弃⽤，不做介绍)
SqlSession是MyBatis中⽤于和数据库交互的顶层类，通常将它与ThreadLocal绑定，⼀个会话使⽤⼀
个SqlSession,并且在使⽤完毕后需要close

#### Executor

Executor也是⼀个接⼝，他有三个常⽤的实现类：
BatchExecutor (重⽤语句并执⾏批量更新)
ReuseExecutor (重⽤预处理语句 prepared statements)
SimpleExecutor (普通的执⾏器，默认)

Executor.query()⽅法⼏经转折，最后会创建⼀个StatementHandler对象，然后将必要的参数传
递给
StatementHandler，使⽤StatementHandler来完成对数据库的查询，最终返回List结果集。

#### StatementHandler

StatementHandler对象主要完成两个⼯作：

+ 对于JDBC的PreparedStatement类型的对象，创建的过程中，我们使⽤的是SQL语句字符串会包

含若⼲个？占位符，我们其后再对占位符进⾏设值。StatementHandler通过
parameterize(statement)⽅法对 S tatement 进⾏设值；

+ StatementHandler 通过 List query(Statement statement, ResultHandler resultHandler)⽅法来

完成执⾏Statement，和将Statement对象返回的resultSet封装成List；

#### getmapper()

```java
    //DefaultSqlSession 中的 getMapper
    public <T> T getMapper(Class<T> type) {
    return configuration.<T>getMapper(type, this);
    }
    //configuration 中的给 g etMapper
    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
    return mapperRegistry.getMapper(type, sqlSession);
    }
    //MapperRegistry 中的 g etMapper
    public <T> T getMapper(Class<T> type, SqlSession sqlSession) {
    //从 MapperRegistry 中的 HashMap 中拿 MapperProxyFactory
    final MapperProxyFactory<T> mapperProxyFactory =
    (MapperProxyFactory<T>) knownMappers.get(type);
    if (mapperProxyFactory == null) {
    throw new BindingException("Type " + type + " is not known to the
    MapperRegistry.");
    }
    try {
    //通过动态代理⼯⼚⽣成示例。
    return mapperProxyFactory.newInstance(sqlSession);
    } catch (Exception e) {
    throw new BindingException("Error getting mapper instance. Cause:
    " + e, e);
    }
    }
    //MapperProxyFactory 类中的 newInstance ⽅法
    public T newInstance(SqlSession sqlSession) {
    //创建了 JDK动态代理的Handler类
    final MapperProxy<T> mapperProxy = new MapperProxy<>(sqlSession,
    mapperInterface, methodCache);
    //调⽤了重载⽅法
    return newInstance(mapperProxy);
    }
    //MapperProxy 类，实现了 InvocationHandler 接⼝
    public class MapperProxy<T> implements InvocationHandler, Serializable {
    //省略部分源码
    private final SqlSession sqlSession;
    private final Class<T> mapperInterface;
    private final Map<Method, MapperMethod> methodCache;
    //构造，传⼊了 SqlSession，说明每个session中的代理对象的不同的！
    public MapperProxy(SqlSession sqlSession, Class<T> mapperInterface,
    Map<Method, MapperMethod> methodCache) {
      this.sqlSession = sqlSession;
    this.mapperInterface = mapperInterface;
    this.methodCache = methodCache;
    }
    //省略部分源码
    }
```

#### invoke

在动态代理返回了示例后，我们就可以直接调⽤mapper类中的⽅法了，但代理对象调⽤⽅法，执⾏是
在MapperProxy中的invoke⽅法中

#### ⼆级缓存

⼆级缓存构建在⼀级缓存之上，在收到查询请求时，MyBatis ⾸先会查询⼆级缓存，若⼆级缓存未命
中，再去查询⼀级缓存，⼀级缓存没有，再查询数据库。
⼆级缓存------》 ⼀级缓存------》数据库
与⼀级缓存不同，⼆级缓存和具体的命名空间绑定，⼀个Mapper中有⼀个Cache，相同Mapper中的
MappedStatement共⽤⼀个Cache，⼀级缓存则是和 SqlSession 绑定。

我们看到⼀个Mapper.xml只会解析⼀次标签，也就是只创建⼀次Cache对象，放进configuration中，
并将cache赋值给MapperBuilderAssistant.currentCache

#### 延迟加载

它的原理是，使⽤ CGLIB 或 Javassist( 默认 ) 创建⽬标对象的代理对象。当调⽤代理对象的延迟加载属
性的 getting ⽅法时，进⼊拦截器⽅法。⽐如调⽤ a.getB().getName() ⽅法，进⼊拦截器的
invoke(...) ⽅法，发现 a.getB() 需要延迟加载时，那么就会单独发送事先保存好的查询关联 B
对象的 SQL ，把 B 查询上来，然后调⽤ a.setB(b) ⽅法，于是 a 对象 b 属性就有值了，接着完
成 a.getB().getName() ⽅法的调⽤。这就是延迟加载的基本原理
总结：延迟加载主要是通过动态代理的形式实现，通过代理拦截到指定⽅法，执⾏数据加载。

## 四、设计模式

### 构建者模式

Builder模式的定义是"将⼀个复杂对象的构建与它的表示分离，使得同样的构建过程可以创建不同的表
示。”，它属于创建类模式，⼀般来说，如果⼀个对象的构建⽐较复杂，超出了构造函数所能包含的范
围，就可以使⽤⼯⼚模式和Builder模式，相对于⼯⼚模式会产出⼀个完整的产品，Builder应⽤于更加
复杂的对象的构建，甚⾄只会构建产品的⼀个部分，直⽩来说，就是使⽤多个简单的对象⼀步⼀步构建
成⼀个复杂的对象

#### Mybatis中的体现

Mybatis的初始化⼯作⾮常复杂，不是只⽤⼀个构造函数就能搞定的。所以使⽤了建造者模式，使⽤了
⼤ 量的Builder，进⾏分层构造，核⼼对象Configuration使⽤了 XmlConfigBuilder来进⾏构造。在Mybatis环境的初始化过程中，SqlSessionFactoryBuilder会调⽤XMLConfigBuilder读取所有的
MybatisMapConfig.xml 和所有的 *Mapper.xml ⽂件，构建 Mybatis 运⾏的核⼼对象 Configuration
对 象，然后将该Configuration对象作为参数构建⼀个SqlSessionFactory对象。

![构建者](构建者.png)

### 工厂模式

在Mybatis中⽐如SqlSessionFactory使⽤的是⼯⼚模式，该⼯⼚没有那么复杂的逻辑，是⼀个简单⼯⼚
模式。
简单⼯⼚模式(Simple Factory Pattern)：⼜称为静态⼯⼚⽅法(Static Factory Method)模式，它属于创
建型模式。
在简单⼯⼚模式中，可以根据参数的不同返回不同类的实例。简单⼯⼚模式专⻔定义⼀个类来负责创建
其他类的实例，被创建的实例通常都具有共同的⽗类

#### MyBatis体现

Mybatis中执⾏Sql语句、获取Mappers、管理事务的核⼼接⼝SqlSession的创建过程使⽤到了⼯⼚模
式。
有⼀个 SqlSessionFactory 来负责 SqlSession 的创建
SqlSessionFactory
可以看到，该Factory的openSession ()⽅法重载了很多个，分别⽀
持autoCommit、Executor、Transaction等参数的输⼊，来构建核⼼的SqlSession对象。

![工厂模式](工厂模式.png)

### 代理模式

代理模式(Proxy Pattern):给某⼀个对象提供⼀个代理，并由代理对象控制对原对象的引⽤。代理模式
的英⽂叫做Proxy，它是⼀种对象结构型模式，代理模式分为静态代理和动态代理，我们来介绍动态代
理

#### MyBatis体现

代理模式可以认为是Mybatis的核⼼使⽤的模式，正是由于这个模式，我们只需要编写Mapper.java接
⼝，不需要实现，由Mybati s后台帮我们完成具体SQL的执⾏。
当我们使⽤Configuration的getMapper⽅法时，会调⽤mapperRegistry.getMapper⽅法，⽽该⽅法⼜
会调⽤ mapperProxyFactory.newInstance(sqlSession)来⽣成⼀个具体的代理：

```java
public class MapperProxyFactory<T> {
private final Class<T> mapperInterface;
private final Map<Method, MapperMethod> methodCache = new
ConcurrentHashMap<Method, MapperMethod>();
public MapperProxyFactory(Class<T> mapperInterface) {
this.mapperInterface = mapperInterface;
}
public Class<T> getMapperInterface() {
return mapperInterface;
}
public Map<Method, MapperMethod> getMethodCache() {
return methodCache;
@SuppressWarnings("unchecked")
protected T newInstance(MapperProxy<T> mapperProxy) {
return (T)
Proxy.newProxyInstance(mapperInterface.getClassLoader(), new
Class[] { mapperInterface },
mapperProxy);
}
public T newInstance(SqlSession sqlSession) {
final MapperProxy<T> mapperProxy = new MapperProxy<T>(sqlSession,
mapperInterface, methodCache);
return newInstance(mapperProxy);
}
}
```

在这⾥，先通过T newInstance(SqlSession sqlSession)⽅法会得到⼀个MapperProxy对象，然后调⽤T
newInstance(MapperProxy mapperProxy)⽣成代理对象然后返回。⽽查看MapperProxy的代码，可
以看到如下内容：

```java
public class MapperProxy<T> implements InvocationHandler, Serializable {
@Override
public Object invoke(Object proxy, Method method, Object[] args) throws
Throwable {
  try {
  if (Object.class.equals(method.getDeclaringClass())) {
  return method.invoke(this, args);
  } else if (isDefaultMethod(method)) {
  return invokeDefaultMethod(proxy, method, args);
  }
  } catch (Throwable t) {
throw ExceptionUtil.unwrapThrowable(t);
  }
final MapperMethod mapperMethod = cachedMapperMethod(method);
return mapperMethod.execute(sqlSession, args);
}
```

