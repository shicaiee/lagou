import com.lagou.edu.dao.ResumeDao;
import com.lagou.edu.pojo.Resume;
import org.hibernate.hql.spi.id.cte.CteValuesListBulkIdStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
public class ResumeDaoTest {


    @Autowired
    private ResumeDao resumeDao;

    /**
     * dao层接口调用分成两块：
     *  1.基础的增删改查
     *  2.专门针对查询的详细分析使用
     */
    @Test
    public void test(){
        Optional<Resume> optional = resumeDao.findById(1l);
        Resume resume = optional.get();
        System.out.println(resume);
    }


    @Test
    public void testFindOne(){
        Resume resume = new Resume();
        resume.setId(1l);
        resume.setName("张三");
        Example<Resume> example = Example.of(resume);
        Optional<Resume> optional = resumeDao.findOne(example);
        Resume resume1 = optional.get();
        System.out.println(resume1);
    }

    @Test
    public void testSave(){
        //新增和更新都是用save 通过传入对象的主键有无来区分
        Resume resume = new Resume();
        resume.setId(4l);
        resume.setName("赵六六");
        resume.setAddress("成都");
        resume.setPhone("13200000000");
        Resume save = resumeDao.save(resume);
        System.out.println(save);
    }

    @Test
    public void testDelete(){
        resumeDao.deleteById(4l);
    }

    @Test
    public void testFindAll(){
        List<Resume> all = resumeDao.findAll();
        System.out.println(all);
    }

    /**
     * 针对查询的使用进行分析
     * 方式一：调用继承的接口中的方法
     * 方式二：可以引用jpql（jpa查询语言）语句进行查询
     * 方式三：可以引用sql语句
     */

    @Test
    public void testJpql(){
        Resume byJpql = resumeDao.findBySql(1l,"张三");
        System.out.println(byJpql);
    }



}
