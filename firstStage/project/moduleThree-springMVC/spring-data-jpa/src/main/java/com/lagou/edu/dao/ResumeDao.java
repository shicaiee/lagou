package com.lagou.edu.dao;

import com.lagou.edu.pojo.Resume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 一个符合SpringDataJpa要求的Dao层接口是需要继承JpaRepository和JpaSpecificationExecutor
 * JpaRepository<操作的实体类类型，主键类型>
 *     封装了基本的的CRUD操作
 *
 * JpaSpecificationExecutor<操作的实体类类型>
 *     封装了复杂的查询（分页，排序等）
 */
public interface ResumeDao extends JpaRepository<Resume,Long>, JpaSpecificationExecutor<Resume> {


    @Query("from Resume where id = ?1 and name =?2")
    Resume findByJpql(Long id,String name);

    /**
     * 使用原生sql语句，需要将nativeQuery属性设置为true，默认为false(jpql)
     * @param id
     * @param name
     * @return
     */
    @Query(value = "select * from tb_resume where id = ?1 and name =?2",nativeQuery = true)
    Resume findBySql(Long id,String name);

}
