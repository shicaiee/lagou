package com.lagou.edu.controller;

import com.lagou.edu.pojo.QueryVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Date;

@Controller
@RequestMapping("demo")
public class DemoController {

    //url: http://localhost:8080/demo/handle01
    @RequestMapping("/handle01")
    public ModelAndView handle01(){
        Date date = new Date();//服务器时间
        //返回服务器时间到前端页面
        //封装了数据和页面信息的ModelAndView
        ModelAndView modelAndView = new ModelAndView();
        // addObject 其实是向请求域中request.setAttribute("date",date)
        modelAndView.addObject("date",date);
        // 视图信息(封装跳转的页面信息)
        modelAndView.setViewName("success");
        return modelAndView;
    }

    /**
     * SpringMVC在handler方法上传入Map,Model和ModelMap参数，并向这些参数中保存数据（放到请求域）
     * 都可以在页面中获取到，那么他们是什么关系呢？
     * 运行时具体类型都是BindingAwareModelMap，相当于给BindingAwareModelMap中保存的数据都会放在请求域中
     *
     * Map(jdk中的接口)
     * Model(spring的接口)
     * ModelMap(class,实现Map接口)
     * BindingAwareModelMap  继承了ExtendedModelMap ，ExtendedModelMap继承了ModelMap，实现了Model接口
     *
     *
     */


    @RequestMapping("/handle02")
    public ModelAndView handle02(@RequestParam QueryVO queryVO){
        System.out.println(queryVO);
        return null;
    }





}
