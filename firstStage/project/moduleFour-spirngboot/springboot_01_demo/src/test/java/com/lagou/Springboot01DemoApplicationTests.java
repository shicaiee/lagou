package com.lagou;

import com.lagou.pojo.Person;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;



@SpringBootTest
@RunWith(SpringRunner.class)
class Springboot01DemoApplicationTests {

    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private Person person;

    @Test
    void contextLoads() {
        System.out.println(person);
    }

    /**
     * 测试日志输出
     * SLF4J 日志级别（从小到大） trace<debug<info<warn<error
     * 日志级别：我们可以手动调整级别
     */
    @Test
    public void testLog(){

        logger.trace("trace 日志");
        logger.debug("debug 日志");
        // springboot中日志的默认级别就是info级别  root级别
        logger.info("info 日志");
        logger.warn("warn 日志");
        logger.error("error 日志");
    }

}
