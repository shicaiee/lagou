package com.lagou.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.DispatcherServlet;

import static org.apache.naming.ContextBindings.getClassLoader;

@Component
public class Devtools implements InitializingBean {
    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("jar:"+ DispatcherServlet.class.getClassLoader().toString());
        System.out.println("自定义:"+ this.getClass().getClassLoader().toString());
    }
}
