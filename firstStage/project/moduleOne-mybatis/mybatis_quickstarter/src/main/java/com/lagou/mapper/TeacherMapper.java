package com.lagou.mapper;

import com.lagou.pojo.Teacher;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/1/27 15:32
 **/
public interface TeacherMapper {

    //@Select("select Tname from teacher")
    //List<Teacher> getTeacherList();


    //@Select("select Tname from teacher where id = #{id} and name = #{name}")
    Teacher getTeacherOne(@Param("id") int id,@Param("name") String name);
}
