package com.lagou.pojo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/1/27 15:42
 **/

public class Teacher {

    private String Tname;

    public String getTname() {
        return Tname;
    }

    public void setTname(String tname) {
        Tname = tname;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "Tname='" + Tname + '\'' +
                '}';
    }
}
