package com.lagou.test;

import com.lagou.mapper.TeacherMapper;
import com.lagou.pojo.Teacher;
import org.apache.ibatis.executor.parameter.ParameterHandler;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/1/27 15:33
 **/
public class MybatisTest {

    @Test
    public void test1() throws IOException {
        // 1.读取配置文件，读成字节输入流
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        // 2.解析配置文件，封装Configuration对象  创建DefaultSqlSessionFactory对象
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        // 3.生产了DefaultSqlSession实例对象， 设置了事务不自动提交，完成了executor对象的创建
        SqlSession sqlSession = build.openSession();
        Teacher teacher = new Teacher();
        // 4. （1）根据statementid来从Configuration中map集合中获取到了指定的MappedStatement对象
        //    （2）将查询任务委派了executor执行器
        sqlSession.selectOne("namespace.id",teacher);
        sqlSession.commit();
//        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
//        Teacher teacherList = mapper.getTeacherOne(11,"liusong");
    }

    @Test
    public void test2() throws IOException {
        // 1.读取配置文件，读成字节输入流
        InputStream resourceAsStream = Resources.getResourceAsStream("sqlMapConfig.xml");
        // 2.解析配置文件，封装Configuration对象  创建DefaultSqlSessionFactory对象
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        // 3.生产了DefaultSqlSession实例对象， 设置了事务不自动提交，完成了executor对象的创建
        SqlSession sqlSession = build.openSession();
        // 4.使用jdk动态代理对mapper接口产生代理对象
        //   代理对象的invoke方法中实际执行的还是sqlSession的具体方法
        TeacherMapper mapper = sqlSession.getMapper(TeacherMapper.class);
        Teacher name = mapper.getTeacherOne(1, "name");
    }


}
