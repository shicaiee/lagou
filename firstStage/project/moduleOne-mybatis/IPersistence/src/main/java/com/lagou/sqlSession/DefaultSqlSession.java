package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;
import com.lagou.pojo.MappedStatement;

import java.lang.reflect.*;
import java.util.List;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/1/22 12:49
 **/
public class DefaultSqlSession implements SqlSession{

    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    /*
    * 利用Executor的实现类去执行query方法
    * */
    @Override
    public <E> List<E> selectList(String statementId, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        List<Object> objects = simpleExecutor.selectList(configuration, mappedStatement, params);
        return (List<E>) objects;
    }

    @Override
    public <T> T selectOne(String statementId, Object... params) throws Exception {
        List<Object> objects = selectList(statementId, params);
        if(objects.size()>0){
            return (T) objects.get(0);
        }else {
            throw new RuntimeException("except one but found "+objects.size());
        }
    }

    @Override
    public <T> int insertOne(String statementId, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        int insert = simpleExecutor.update(configuration, mappedStatement, params);
        return insert;
    }

    @Override
    public <T> int updateOne(String statementId, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        int update = simpleExecutor.update(configuration, mappedStatement, params);
        return update;
    }

    @Override
    public <T> int deleteOne(String statementId, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
        int delete = simpleExecutor.update(configuration, mappedStatement, params);
        return delete;
    }

    @Override
    public <T> T getMapper(Class<?> daoClass) {
        Object proxyInstance = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class<?>[]{daoClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //准备参数
                String className = method.getDeclaringClass().getName();
                String methodName = method.getName();
                //所以 xml中的namespace 就是dao接口的全限定名  而xml中的id 就需要匹配 dao接口的方法名
                String statementId = className + "." + methodName;
                //简化版
                Type genericReturnType = method.getGenericReturnType();
//                if (genericReturnType instanceof ParameterizedType) {
//                    //如果是参数化的就调用 selectList()
//                    return selectList(statementId,args);
//                }
//                return selectOne(statementId,args);
                MappedStatement mappedStatement = configuration.getMappedStatementMap().get(statementId);
                switch (mappedStatement.getType()){
                    case "select":
                        if (genericReturnType instanceof ParameterizedType) {
                            //如果是参数化的就调用 selectList()
                            return selectList(statementId,args);
                        }else return selectOne(statementId,args);
                    case "insert":
                        return insertOne(statementId,args);
                    case "update":
                        return updateOne(statementId,args);
                    case "delete":
                        return deleteOne(statementId,args);
                    default:
                        return null;
                }
            }
        });
        return (T) proxyInstance;
    }
}
