package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;
import com.lagou.pojo.MappedStatement;

import java.sql.SQLException;
import java.util.List;

public interface Executor {

    <E> List<E> selectList(Configuration configuration, MappedStatement mappedStatement,Object... params) throws Exception;


    public int update(Configuration configuration, MappedStatement mappedStatement, Object... params) throws Exception;

}
