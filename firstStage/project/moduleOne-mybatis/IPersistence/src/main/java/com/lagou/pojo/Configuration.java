package com.lagou.pojo;

import lombok.Data;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Data
public class Configuration {

    private DataSource dataSource;

    /*
    * key: statementId value:封装好的mappedStatement对象
    * */
    Map<String,MappedStatement> mappedStatementMap = new HashMap<>();
}
