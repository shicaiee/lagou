package com.lagou.io;

import javax.annotation.Resource;
import java.io.InputStream;

public class Resources {

    //根据配置文件路劲，将配置文件加载成字节输入流，存储在内存中
    public static InputStream getResourcesAsSteam(String path){
        InputStream resourceAsStream = Resources.class.getClassLoader().getResourceAsStream(path);
        return resourceAsStream;
    }
}
