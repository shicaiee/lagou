package com.lagou.sqlSession;

import com.lagou.pojo.Configuration;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/1/22 12:47
 **/
public class DefaultSqlSessionFactory implements SqlSessionFactory{

    private Configuration configuration;

    public DefaultSqlSessionFactory(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public SqlSession openSession() {
        return new DefaultSqlSession(configuration);
    }
}
