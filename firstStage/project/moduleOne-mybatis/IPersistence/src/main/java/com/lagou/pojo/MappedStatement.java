package com.lagou.pojo;

import lombok.Data;

@Data
public class MappedStatement {

    private String id;

    private String type;

    private String resultType;

    private String paramterType;

    private String sql;



}
