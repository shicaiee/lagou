package com.lagou.sqlSession;

import java.util.List;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/1/22 12:48
 **/
public interface SqlSession {

    <E> List<E> selectList(String statementId,Object... params) throws Exception;

    <T> T selectOne(String statementId,Object... params) throws Exception;

    <T> int insertOne(String statementId, Object... params) throws Exception;

    <T> int updateOne(String statementId, Object... params) throws Exception;

    <T> int deleteOne(String statementId, Object... params) throws Exception;

    <T> T getMapper(Class<?> daoClass);
}
