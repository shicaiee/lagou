package com.lagou.dao;

import com.lagou.pojo.User;

import java.util.List;

public interface IUser {

    List<User> selectList();

    User selectOne(User user);

    int insertOne(User user);

    int updateOne(User user);

    int deleteOne(User user);

}
