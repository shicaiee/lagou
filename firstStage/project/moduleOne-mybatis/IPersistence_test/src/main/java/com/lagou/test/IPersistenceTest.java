package com.lagou.test;

import com.lagou.dao.IUser;
import com.lagou.io.Resources;
import com.lagou.pojo.User;
import com.lagou.sqlSession.SqlSession;
import com.lagou.sqlSession.SqlSessionFactory;
import com.lagou.sqlSession.SqlSessionFactoryBuilder;
import org.dom4j.DocumentException;
import org.junit.Test;

import java.beans.PropertyVetoException;
import java.io.InputStream;
import java.io.PushbackReader;
import java.util.List;

public class IPersistenceTest {
    /**
     *  题目分析：现有代码基础上添加、修改及删除功能。【需要采用getMapper方式】
     *      分析现有代码，找出添加、修改、删除与查询的不同之处
     *  实现思路：
     *      使用端
     *          一：IUser 中添加添加、修改、删除的方法
     *          二：UserMapper.xml中添加添加、修改、删除的sql标签
     *      框架端：
     *          一：sqlSession中添加添加、修改、删除方法
     *          二：getMapper()方法中区别所调用的方法  根据MappedStatement中添加type属性，在xml解析时保存
     *          三：Executor的实现类SimpleExecutor中添加添加、修改、删除方法的实现
     */

    @Test
    public void test() throws Exception {
        InputStream resourcesAsSteam = Resources.getResourcesAsSteam("sqlMapConfig.xml");
        SqlSessionFactoryBuilder sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
        SqlSessionFactory build = sqlSessionFactoryBuilder.build(resourcesAsSteam);
        SqlSession sqlSession = build.openSession();
        User user = new User();
        user.setId(4);
        user.setUsername("修改李四");
//        User o = sqlSession.selectOne("User.selectOne", user);
//        System.out.println(o);
//
//        List<Object> objects = sqlSession.selectList("User.selectList");
//        for (Object object : objects) {
//            System.out.println(object);
//        }
        IUser mapper = sqlSession.getMapper(IUser.class);
        //List<User> users = sqlSession.selectList("com.lagou.dao.IUser.selectList", null);
//        List<User> users = mapper.selectList();
//        users.forEach(c->{
//            System.out.println(c);
//        });
        //User user1 = mapper.selectOne(user);
        //System.out.println(user1);
        int i = mapper.deleteOne(user);
    }
}
