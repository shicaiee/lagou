package com.lagou.edu.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

/**
 * 用来开启，提交，回滚事务
 */
@Component
public class TransactionManager {

    @Autowired
    private ConnectionUtils connectionUtils;


    /**
     * 开启事务
     */
    public void startTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().setAutoCommit(false);
    }

    /**
     * 提交事务
     */
    public void commitTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().commit();
    }

    /**
     * 回滚事务
     */
    public void rollbackTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().rollback();
    }







}
