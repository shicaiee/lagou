package com.lagou.edu.utils;

import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Service;

import java.sql.SQLException;

/**
 * 用来开启，提交，回滚事务
 */
@Service(value = "transactionManager")
public class TransactionManager {

    @Autowired
    private ConnectionUtils connectionUtils;

//    public void setConnectionUtils(ConnectionUtils connectionUtils){
//        this.connectionUtils = connectionUtils;
//    }

    /**
     * 开启事务
     */
    public void startTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().setAutoCommit(false);
    }

    /**
     * 提交事务
     */
    public void commitTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().commit();
    }

    /**
     * 回滚事务
     */
    public void rollbackTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().rollback();
    }







}
