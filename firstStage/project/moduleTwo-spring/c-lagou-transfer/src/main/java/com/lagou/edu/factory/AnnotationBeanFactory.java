package com.lagou.edu.factory;

import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Service;
import com.lagou.edu.annotation.Transactional;
import com.lagou.edu.utils.ClassUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *  题目：学员自定义@Service、@Autowired、@Transactional注解类，完成基于注解的IOC容器（Bean对象创建及依赖注入维护）和声明式事务控制，写到转账工程中，并且可以实现转账成功和转账异常时事务回滚
 *        注意考虑以下情况：
 *        1）注解有无value属性值【@service（value=""） @Repository（value=""）】
 *        2）service层是否实现接口的情况【jdk还是cglib】
 *  题目分析：
 *      将BeanFactory的解析xml文件的代码，改造成解析注解
 *      步骤：
 *          1.创建注解 @interface  Autowired  Service  Transactional
 *          2.改造BeanFactory的静态代码块
 *           2.1 扫描所有的Service注解，并实例化对象
 *           2.2 扫描所有的AutoWired注解，并利用反射注入属性值
 *           2.3 扫描所有的Transactional注解,生产代理对象
 *
 */
public class AnnotationBeanFactory {

    private static HashMap<String, Object> map = new HashMap<String, Object>();

    public static Object getBeanById(String id) {
        return map.get(id);
    }

    /**
     * 静态方法一开始就解析带有自定义注解的Bean
     */
    static {
        Set<Class<?>> classes = ClassUtils.getClasses("com.lagou.edu", true);
        if (classes.size() > 0) {

            //扫描自定义 @Service的注解
            for (Class<?> clazz : classes) {
                Service declaredAnnotation = clazz.getDeclaredAnnotation(Service.class);
                if (declaredAnnotation != null) {
                    // 获value值，也就是该bean的id
                    String value = declaredAnnotation.value();
                    //如果没有，则默认为驼峰命名
                    if ("".equals(value.trim())) {
                        value = clazz.getInterfaces()[0].getSimpleName();
                    }
                    //实例化该对象
                    Object bean = null;
                    try {
                        bean = clazz.newInstance();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    map.put(value, bean);
                }
            }

            //扫描自定义注解@AutoWired,然后将其注入
            System.out.println("扫描自定义注解@AutoWired,然后将其注入");
            Set<Map.Entry<String, Object>> entrySet = map.entrySet();
            for (Map.Entry<String, Object> entry : entrySet) {
                Object value = entry.getValue();
                Class<?> valueClass = value.getClass();
                Field[] declaredFields = valueClass.getDeclaredFields();
                for (Field field : declaredFields) {
                    Autowired declaredAnnotation = field.getDeclaredAnnotation(Autowired.class);
                    //若属性带有AutoWired 则根据名称注入
                    if (declaredAnnotation != null) {
                        String beanName = declaredAnnotation.value();
                        if ("".equals(beanName.trim())) {
                            //默认
                            beanName = field.getName();
                        }
                        Object bean = map.get(beanName);
                        field.setAccessible(true);
                        try {
                            field.set(value, bean);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }


            //扫描自定义注解@Transational,然后将其生成代理对象
            //Transactional  注解实际上是使原来的类有了增强的属性
            for (Class<?> clazz : classes) {
                Transactional transactional = clazz.getDeclaredAnnotation(Transactional.class);
                if (transactional != null) {
                    Service serviceAnnotation = clazz.getDeclaredAnnotation(Service.class);
                    //获取原对象
                    String value = serviceAnnotation.value();
                    if ("".equals(value.trim())) {
                        value = clazz.getInterfaces()[0].getSimpleName();
                    }
                    Object bean = map.get(value);//被代理对象
                    //转为代理对象
                    ProxyFactory proxyFactory = (ProxyFactory) map.get("proxyFactory");
                    //判断是否实现接口 实现接口使用JDK动态代理  没有实现接口使用cglib动态代理
                    Class<?>[] interfaces = clazz.getInterfaces();
                    Object proxy = null;
                    if (interfaces.length > 0) {
                        proxy = proxyFactory.getJDKProxy(bean);//代理对象
                    } else proxy = proxyFactory.getCglibProxy(bean);
                    //代理对象替换原对象
                    map.put(value, proxy);
                }

            }

        }


    }


}
