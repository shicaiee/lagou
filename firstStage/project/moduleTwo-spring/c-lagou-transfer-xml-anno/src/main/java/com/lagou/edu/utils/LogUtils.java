package com.lagou.edu.utils;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogUtils {

    @Pointcut("execution(public void com.lagou.edu.service.impl.TransferServiceImpl.transfer(java.lang.String,java.lang.String,int))")
    public void pt1(){

    }

    @Before("pt1()")
    public void beforeMethod(JoinPoint joinPoint){
        //获取参数
        joinPoint.getArgs();
        System.out.println("业务逻辑开始之前执行");
    }


    @After("pt1()")
    public void afterMethod(){
        System.out.println("业务逻辑结束时执行，无论异常与否");
    }


    @AfterThrowing("pt1()")
    public void exceptionMethod(){
        System.out.println("业务逻辑异常时执行");
    }

    @AfterReturning(value = "pt1()",returning = "retValue")
    public void successMethod(Object retValue){
        System.out.println("业务逻辑正常时执行");
    }

    //@Around("pt1()")
    public void arroundMethod(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        System.out.println("业务逻辑环绕前执行");
        //执行目标方法
        proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());
        System.out.println("业务逻辑环绕后执行");
    }

}
