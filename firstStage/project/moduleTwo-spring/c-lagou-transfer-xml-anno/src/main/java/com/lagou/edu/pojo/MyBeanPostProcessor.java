package com.lagou.edu.pojo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/2/11 10:05
 **/
@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        if("lazyResult".equals(beanName)){
            System.out.println("第四步：调用了BeanPostProcessor的postProcessBeforeInitialization方法");
        }
        return null;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if("lazyResult".equals(beanName)){
            System.out.println("第七步：调用了BeanPostProcessor的postProcessAfterInitialization方法");
        }
        return null;
    }
}
