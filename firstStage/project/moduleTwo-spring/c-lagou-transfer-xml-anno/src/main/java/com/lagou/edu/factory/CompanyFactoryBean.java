package com.lagou.edu.factory;

import com.lagou.edu.pojo.Company;
import org.springframework.beans.factory.FactoryBean;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/2/10 19:21
 **/
public class CompanyFactoryBean implements FactoryBean<Company> {


    private String companyInfo;

    public void setCompanyInfo(String companyInfo) {
        this.companyInfo = companyInfo;
    }

    @Override
    public Company getObject() throws Exception {
        Company company = new Company();
        String[] info = companyInfo.split(",");
        company.setName(info[0]);
        company.setAddress(info[1]);
        company.setScale(info[2]);
        return company;
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }
}
