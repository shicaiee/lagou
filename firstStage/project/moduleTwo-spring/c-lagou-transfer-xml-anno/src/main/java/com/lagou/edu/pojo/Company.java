package com.lagou.edu.pojo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/2/10 19:19
 **/
public class Company {

    private String name;

    private String address;

    private String scale;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale;
    }

    @Override
    public String toString() {
        return "Company{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", scale='" + scale + '\'' +
                '}';
    }
}
