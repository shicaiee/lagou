package com.lagou.edu.utils;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.pool.DruidPooledConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
@Component
public class ConnectionUtils {

    private ThreadLocal<Connection> connections = new ThreadLocal<>();

    @Autowired
    private DruidDataSource druidDataSource;


    /**
     * 新增获取线程的方法
     */
    public Connection getCurrentConnection() throws SQLException {
        Connection connection = connections.get();
        if(connection == null){
            connection = druidDataSource.getConnection();
            connections.set(connection);
        }
        return connection;
    }





}
