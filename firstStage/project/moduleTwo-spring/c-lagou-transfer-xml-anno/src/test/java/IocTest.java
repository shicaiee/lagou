import com.lagou.edu.service.TransferService;
import jdk.nashorn.internal.ir.CallNode;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/2/10 18:32
 **/
public class IocTest {

    @Test
    public void test1(){
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        //Object lazyResult = classPathXmlApplicationContext.getBean("lazyResult");
        //System.out.println(lazyResult);
        //Object company = classPathXmlApplicationContext.getBean("company");
        //Object companyFactoryBean = classPathXmlApplicationContext.getBean("&company");
        //System.out.println(company);
        //System.out.println(companyFactoryBean);
        Object lazyResult = applicationContext.getBean("lazyResult");
        System.out.println(lazyResult);

    }

    @Test
    public void test2() throws Exception {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");
        TransferService bean = applicationContext.getBean(TransferService.class);
        bean.transfer("6029621011001","6029621011000",100);
    }

}
