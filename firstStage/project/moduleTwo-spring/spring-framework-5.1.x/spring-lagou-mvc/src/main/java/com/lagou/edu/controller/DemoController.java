package com.lagou.edu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Controller
@RequestMapping("/demo")
public class DemoController {


	/**
	 *
	 *
	 */
	@RequestMapping("/handler01")
	public String handler01(String name, Map<String,Object> model){
		System.out.println("处理中");
		return "success";
	}

}
