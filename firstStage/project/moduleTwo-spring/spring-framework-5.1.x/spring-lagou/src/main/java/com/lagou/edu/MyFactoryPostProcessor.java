package com.lagou.edu;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/2/13 14:04
 **/
public class MyFactoryPostProcessor implements BeanFactoryPostProcessor {

	public MyFactoryPostProcessor() {
		System.out.println("工厂后置处理器的构造方法");
	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		System.out.println("工厂后置处理器的后置处理方法");
	}
}
