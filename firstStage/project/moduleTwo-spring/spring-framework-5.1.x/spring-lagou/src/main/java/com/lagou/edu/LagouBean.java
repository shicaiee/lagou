package com.lagou.edu;

import org.springframework.beans.factory.InitializingBean;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/2/13 11:16
 **/
public class LagouBean implements InitializingBean {

	private ItBean itBean;

	public void setItBean(ItBean itBean) {
		this.itBean = itBean;
	}

	public LagouBean() {
		System.out.println("bean构造方法");
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("afterPropertiesSet");
	}
}
