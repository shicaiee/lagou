import com.lagou.edu.LagouBean;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/2/13 11:21
 **/
public class ContextTest {


	/**
	 * Ioc 容器源码分析基础案例
	 */
	@Test
	public void testIoC(){
		/**
		 * ApplicationContext是容器的高级接口，BeanFactory(顶级容器/根容器，规范了/定义了容器的基础行为)
		 * ApplicationContext 是Spring 应用上下文，官方称为Ioc容器（错误的认识：容器就是map而已，准确来说，map是ioc容器的一个成员），
		 * map 叫做单例池，singletonObjects,容器是一组组件和过程的集合，包括BeanFactory，单例池，BeanPostProcessor等之间的协作流程
		 *
		 *  构造器执行，初始化方法执行，Bean后置处理器的before/after方法：AbstractApplicationContext # refresh # finishBeanFactoryInitialization
		 *	Bean工厂后置处理器初始化，方法执行： AbstractApplicationContext # refresh # invokeBeanFactoryPostProcessors
		 *	Bean后置处理器初始化：AbstractApplicationContext # refresh # registerBeanPostProcessors
		 */
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		System.out.println(context.getBean(LagouBean.class));
	}


}
