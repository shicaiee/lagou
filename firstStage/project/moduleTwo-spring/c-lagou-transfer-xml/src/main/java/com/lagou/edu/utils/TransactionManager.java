package com.lagou.edu.utils;

import java.sql.SQLException;

/**
 * 用来开启，提交，回滚事务
 */
public class TransactionManager {

    private ConnectionUtils connectionUtils;

    public void setConnectionUtils(ConnectionUtils connectionUtils){
        this.connectionUtils = connectionUtils;
    }

    /**
     * 开启事务
     */
    public void startTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().setAutoCommit(false);
    }

    /**
     * 提交事务
     */
    public void commitTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().commit();
    }

    /**
     * 回滚事务
     */
    public void rollbackTransaction() throws SQLException {
        connectionUtils.getCurrentConnection().rollback();
    }







}
