package com.lagou.edu.factory;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.lagou.edu.utils.TransactionManager;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 用来生产代理对象
 */
public class ProxyFactory {

    private TransactionManager transactionManager;

    public void setTransactionManager(TransactionManager transactionManager){
        this.transactionManager = transactionManager;
    }



    public Object getJDKProxy(Object obj){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), obj.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //在这处理事务，增强业务逻辑
                Object result = null;
                try{
                    transactionManager.startTransaction();
                    result = method.invoke(obj,args);
                    transactionManager.commitTransaction();
                }catch (Exception e){
                    e.printStackTrace();
                    transactionManager.rollbackTransaction();
                    throw e;
                }
                return result;
            }
        });

    }




}
