//package com.lagou.edu.factory;
//
//import org.dom4j.Document;
//import org.dom4j.DocumentException;
//import org.dom4j.Element;
//import org.dom4j.io.SAXReader;
//
//import java.io.InputStream;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.util.HashMap;
//import java.util.List;
//
//public class BeanFactory {
//
//
//    private static HashMap<String, Object> map = new HashMap<String, Object>();
//
//    public static Object getBeanById(String id) {
//        return map.get(id);
//    }
//
//    /**
//     * 静态方法一开始就解析xml文件中的bean
//     */
//    static {
//        InputStream resourceAsStream = BeanFactory.class.getClassLoader().getResourceAsStream("beans.xml");
//        //利用dom4j 解析
//        SAXReader saxReader = new SAXReader();
//        Document document = null;
//        try {
//            document = saxReader.read(resourceAsStream);
//
//            Element rootElement = document.getRootElement();
//            List<Element> elementList = rootElement.selectNodes("//bean");
//            for (Element element : elementList) {
//                //获取其id属性和class属性
//                String id = element.attributeValue("id");
//                String clazz = element.attributeValue("class");
//                //查看静态map中是否含有该id的bean对象
//                Object o = map.get(id);
//                if(o == null){
//                    //利用反射创建,然后放入map
//                    o = Class.forName(clazz).newInstance();
//                    map.put(id,o);
//                }
//            }
//            //在解决对象的依赖问题
//            List<Element> propertyList = rootElement.selectNodes("//property");
//            for (Element element : propertyList) {
//                //获取其映射 和set方法名
//                String ref = element.attributeValue("ref");
//                String name = element.attributeValue("name");
//                //获取其父级节点
//                Element parent = element.getParent();
//                String pId = parent.attributeValue("id");
//                Object o = map.get(pId);
//                Method[] methods = o.getClass().getMethods();
//                for (Method method : methods) {
//                    if(method.getName().equals("set".concat(name))){
//                        method.invoke(o,map.get(ref));
//                    }
//                }
//                //然后在将父级节点放回map
//                map.put(pId,o);
//            }
//        } catch (DocumentException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InstantiationException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        }
//
//    }
//
//}
