package com.lagou.edu.factory;

import com.fasterxml.jackson.annotation.JsonIgnoreType;
import com.lagou.edu.annotation.Autowired;
import com.lagou.edu.annotation.Service;
import com.lagou.edu.utils.TransactionManager;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * 用来生产代理对象
 */
@Service(value = "proxyFactory")
public class ProxyFactory {

    @Autowired(value = "transactionManager")
    private TransactionManager transactionManager;

//    public void setTransactionManager(TransactionManager transactionManager){
//        this.transactionManager = transactionManager;
//    }



    public Object getJDKProxy(Object obj){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(), obj.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //在这处理事务，增强业务逻辑
                Object result = null;
                try{
                    transactionManager.startTransaction();
                    result = method.invoke(obj,args);
                    transactionManager.commitTransaction();
                }catch (Exception e){
                    e.printStackTrace();
                    transactionManager.rollbackTransaction();
                    throw e;
                }
                return result;
            }
        });
    }


    public Object getCglibProxy(Object obj) {
        return  Enhancer.create(obj.getClass(), new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                Object result = null;
                try{
                    // 开启事务(关闭事务的自动提交)
                    transactionManager.startTransaction();

                    result = method.invoke(obj,objects);

                    // 提交事务

                    transactionManager.commitTransaction();
                }catch (Exception e) {
                    e.printStackTrace();
                    // 回滚事务
                    transactionManager.rollbackTransaction();

                    // 抛出异常便于上层servlet捕获
                    throw e;

                }
                return result;
            }
        });
    }




}
