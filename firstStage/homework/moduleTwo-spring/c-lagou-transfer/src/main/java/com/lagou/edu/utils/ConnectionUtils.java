package com.lagou.edu.utils;

import com.alibaba.druid.pool.DruidPooledConnection;
import com.lagou.edu.annotation.Service;

import java.sql.Connection;
import java.sql.SQLException;

@Service("connectionUtils")
public class ConnectionUtils {

    private ThreadLocal<Connection> connections = new ThreadLocal<>();

    /**
     * 新增获取线程的方法
     */
    public Connection getCurrentConnection() throws SQLException {
        Connection connection = connections.get();
        if(connection == null){
            connection = DruidUtils.getInstance().getConnection();
            connections.set(connection);
        }
        return connection;
    }





}
