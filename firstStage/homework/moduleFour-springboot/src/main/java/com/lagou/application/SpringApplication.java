package com.lagou.application;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.LifecycleListener;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/10 22:41
 **/
public class SpringApplication {

    /**
     * 主方法，创建tomcat并运行
     */
    public static void run() {
        Tomcat tomcat = new Tomcat();
        System.out.println(tomcat);
        tomcat.setPort(8088);
        try {
            File tempDir = File.createTempFile(tomcat + ".", ".80");
            tempDir.delete();
            tempDir.mkdir();
            tempDir.deleteOnExit();
            Context context = tomcat.addContext("/", tempDir.getAbsolutePath());
            context.addLifecycleListener((LifecycleListener) Class.forName(tomcat.getHost().getConfigClass())
                    .getConstructor().newInstance());
            tomcat.start();
            System.out.println("tomcat启动成功");
            tomcat.getServer().await();
        } catch (Exception e) {
            System.out.println("启动失败");
            e.printStackTrace();
        }
    }
}
