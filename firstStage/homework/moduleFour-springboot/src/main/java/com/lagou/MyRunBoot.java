package com.lagou;

import com.lagou.application.SpringApplication;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/10 22:48
 **/
public class MyRunBoot {
    public static void main(String[] args) {
        SpringApplication.run();
    }
}
