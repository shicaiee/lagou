package com.lagou.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/10 22:47
 **/
@Configuration
@ComponentScan(basePackages = "com.lagou")
public class AppConfig {
}
