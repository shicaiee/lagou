package com.lagou.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/11 0:01
 **/
@RestController
public class MyController {

    @RequestMapping("/test")
    public String test(){
        return "hello";
    }
}
