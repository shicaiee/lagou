package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE) //使用在类上
public @interface LagouController {

    String value() default "";

}
