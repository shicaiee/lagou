package com.lagou.edu.mvcframework.servlet;

import com.lagou.edu.mvcframework.annotations.*;
import com.lagou.edu.mvcframework.pojo.Handler;
import jdk.nashorn.internal.ir.CallNode;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LgDispatcherServlet extends HttpServlet {


    private Properties properties = new Properties();


    private Map<String,Object> ioc = new HashMap<>();


    //private Map<String,Method> handlerMapping = new HashMap<>(); //存储url和method之间的映射关系

    private List<Handler> handlerMapping = new ArrayList<>();

    /**
     * 手写MVC框架基础上增加如下功能
     *
     * 1）定义注解@Security（有value属性，接收String数组），该注解用于添加在Controller类或者Handler方法上，表明哪些用户拥有访问该Handler方法的权限（注解配置用户名）
     *
     * 2）访问Handler时，用户名直接以参数名username紧跟在请求的url后面即可，比如http://localhost:8080/demo/handle01?username=zhangsan
     *
     * 3）程序要进行验证，有访问权限则放行，没有访问权限在页面上输出
     *
     * 注意：自己造几个用户以及url，上交作业时，文档提供哪个用户有哪个url的访问权限
     */

    @Override
    public void init(ServletConfig config) throws ServletException {
        // 1.加载配置文件 springmvc.properties
        String contextConfigLocation = config.getInitParameter("contextConfigLocation");
        doLoadConfig(contextConfigLocation);
        // 2.扫描相关的类，扫面注解
        doScan(properties.getProperty("scanPackage"));
        // 3.初始化bean对象（实现ioc容器，基于注解）
        doInstance();
        // 4.实现依赖注入
        doAutowired();
        // 5.构造一个HandlerMapping处理器映射器，将配置好的url和Method建立映射关系
        initHandlerMapping();
        System.out.println("lagou mvc 初始化完成...");
        // 6.等待请求进入，处理请求
    }

    /**
     * 最关键环节
     * 目的：将url和method建立关联
     */
    private void initHandlerMapping() {
        if(ioc.isEmpty()) return;
        for (Map.Entry<String,Object> entry: ioc.entrySet()){
            Class<?> aClass = entry.getValue().getClass();
            if(!aClass.isAnnotationPresent(LagouController.class)) continue;
            String baseUrl = "";
            if(aClass.isAnnotationPresent(LagouRequestMapping.class)){
                LagouRequestMapping annotation = aClass.getAnnotation(LagouRequestMapping.class);
                baseUrl = annotation.value();
            }
            //获取handler的权限
            List<String> security = new ArrayList<>();
            if(aClass.isAnnotationPresent(Security.class)){
                Security annotation = aClass.getAnnotation(Security.class);
                security = Arrays.asList(annotation.value());
            }
            //获取方法
            Method[] methods = aClass.getMethods();
            for (int i = 0; i < methods.length; i++) {
                Method method = methods[i];
                if(!method.isAnnotationPresent(LagouRequestMapping.class)) continue;
                LagouRequestMapping annotation = method.getAnnotation(LagouRequestMapping.class);
                String methodUrl = annotation.value();
                String url = baseUrl+methodUrl;
                // 把method所有信息封装成 Handler
                Handler handler = new Handler(entry.getValue(), method, Pattern.compile(url));
                // 计算方法的参数位置信息
                Parameter[] parameters = method.getParameters();
                for (int j = 0; j < parameters.length; j++) {
                    Parameter parameter = parameters[j];
                    if(parameter.getType() == HttpServletRequest.class || parameter.getType() == HttpServletResponse.class){
                        handler.getParamIndexMapping().put(parameter.getType().getSimpleName(),j);
                    }else {
                        handler.getParamIndexMapping().put(parameter.getName(),j);
                    }
                }
                //获取方法上的权限
                if(method.isAnnotationPresent(Security.class)) {
                    security = Arrays.asList(method.getAnnotation(Security.class).value());
                }
                handler.setSecurity(security);
                handlerMapping.add(handler);
            }
        }

    }

    private void doAutowired() {
        if(ioc.isEmpty()) return;
        //有对象，再进行依赖处理
        //遍历ioc所有对象，查看对象中所有字段 查看是否有 @LagouAutowired注解
        for (Map.Entry<String,Object> entry: ioc.entrySet()){
            //bean对象中的字段信息
            Field[] declaredFields = entry.getValue().getClass().getDeclaredFields();
            for (int i = 0; i < declaredFields.length; i++) {
                Field declaredField = declaredFields[i];
                if(!declaredField.isAnnotationPresent(LagouAutoWired.class)){
                    continue;
                }
                //有该注解
                LagouAutoWired annotation = declaredField.getAnnotation(LagouAutoWired.class);
                String beanName = annotation.value();//需要注入bean的id
                if("".equals(beanName.trim())){
                    beanName = declaredField.getType().getName();
                }
                //开启赋值
                declaredField.setAccessible(true);
                try {
                    declaredField.set(entry.getValue(),ioc.get(beanName));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 基于classNames 缓存的类的权限定类名，以及反射技术，完成对象创建和管理
     */
    private void doInstance(){
        if(classNames.size() == 0) return;
        for (int i = 0; i < classNames.size(); i++) {
            String className = classNames.get(i);
            //反射
            try {
                Class<?> aClass = Class.forName(className);
                //区分controller 和 service
                if(aClass.isAnnotationPresent(LagouController.class)){
                    String simpleName = aClass.getSimpleName(); //DemoController
                    String lowFirstSimpleName = lowFirst(simpleName);
                    Object o = aClass.newInstance();
                    ioc.put(lowFirstSimpleName,o);
                }else if(aClass.isAnnotationPresent(LagouService.class)){
                    LagouService annotation = aClass.getAnnotation(LagouService.class);
                    String beanName = annotation.value();
                    if("".equals(beanName)){
                        //没有指定 就以类名首字母小写
                        beanName = lowFirst(aClass.getSimpleName());
                    }
                    ioc.put(beanName,aClass.newInstance());
                    //service 往往是有接口的 此时再以接口名为id 放入一份对象到ioc 便于后期根据接口类型注入
                    Class<?>[] interfaces = aClass.getInterfaces();
                    for (int i1 = 0; i1 < interfaces.length; i1++) {
                        ioc.put(interfaces[i1].getName(),aClass.newInstance());
                    }
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    public String lowFirst(String str){
        char[] chars = str.toCharArray();
        if('A'<= chars[0] && chars[0] <= 'Z'){
            chars[0] += 32;
        }
        return String.valueOf(chars);
    }

    private List<String> classNames = new ArrayList<>(); //缓存扫描到的类的权限定类名

    private void doScan(String scanPackage) {
        String scanPackagePath = Thread.currentThread().getContextClassLoader().getResource("").getPath()+ scanPackage.replaceAll("\\.", "/");
        File pack = new File(scanPackagePath);
        File[] files = pack.listFiles();
        for (File file: files){
            if(file.isDirectory()){
                //递归
                doScan(scanPackage+"."+file.getName());
            }else if(file.getName().endsWith(".class")){
                String className = scanPackage+"."+file.getName().replaceAll(".class","");
                classNames.add(className);
            }
        }
    }

    private void doLoadConfig(String contextConfigLocation){
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream(contextConfigLocation));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //处理请求，根据url 找到对应的Method方法，进行调用
        //获取uri
        //String requestURI = req.getRequestURI();
        //Method method = handlerMapping.get(requestURI);
        //反射调用 需要传入对象，需要传入参数  此处无法完成调用 没有缓存对象 也没有参数！！！改造initHandlerMapping()方法


        //根据uri 获取当前请求的handler
        Handler handler = getHandler(req);
        if(handler == null){
            resp.getWriter().write("404 not found");
            return;
        }
        //如果权限不匹配，返回提示信息
        if(!handler.getSecurity().contains(req.getParameter("name"))){
            resp.getWriter().write("has no permission");
            return;
        }
        //参数绑定
        //获取所有参数类型数组，这个数组的长度 就是我们最后要传入的 args 数组的长度
        Class<?>[] parameterTypes = handler.getMethod().getParameterTypes();
        Object[] paraValues = new Object[parameterTypes.length];

        Map<String, String[]> parameterMap = req.getParameterMap();
        for(Map.Entry<String,String[]> param: parameterMap.entrySet()){
            String value = StringUtils.join(param.getValue(), ",");
            //如果参数和方法中的参数匹配上了，填充数据
            if(!handler.getParamIndexMapping().containsKey(param.getKey())) continue;
            //方法形参确实有该参数，找到它的位置
            Integer index = handler.getParamIndexMapping().get(param.getKey());
            paraValues[index] = value; //把前台传递过来的参数值填充到对应的位置去
        }

        int requestIndex = handler.getParamIndexMapping().get(HttpServletRequest.class.getSimpleName());
        paraValues[requestIndex] = req;

        int responseIndex = handler.getParamIndexMapping().get(HttpServletResponse.class.getSimpleName());
        paraValues[responseIndex] = resp;
        //最终调用handler 的method
        try {
            handler.getMethod().invoke(handler.getController(),paraValues);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private Handler getHandler(HttpServletRequest req) {

        if(handlerMapping.isEmpty()) return null;
        String url = req.getRequestURI();
        for (Handler handler:handlerMapping) {
            Matcher matcher = handler.getPattern().matcher(url);
            if(!matcher.matches()) continue;
            return handler;
        }
        return null;
    }
}
