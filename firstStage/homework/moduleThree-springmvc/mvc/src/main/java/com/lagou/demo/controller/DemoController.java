package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutoWired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@LagouController
@LagouRequestMapping("/demo")
public class DemoController {

    @LagouAutoWired
    private IDemoService demoService;

    /**
     *  URL: /demo/query
     * @param request
     * @param response
     * @param name
     * @return
     */
    @LagouRequestMapping("/query")
    @Security(value = {"zhangsan","lisi"})
    public String query(HttpServletRequest request, HttpServletResponse response,String name){
        return demoService.get(name);
    }


    @LagouRequestMapping("/insert")
    @Security(value = {"wangwu","lisi"})
    public String insert(HttpServletRequest request, HttpServletResponse response,String name){
        return demoService.get(name);
    }
}
