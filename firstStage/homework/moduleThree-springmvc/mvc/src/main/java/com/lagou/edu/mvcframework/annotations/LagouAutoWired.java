package com.lagou.edu.mvcframework.annotations;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD) // 使用在字段上
public @interface LagouAutoWired {

    String value() default "";

}
