###什么是tomcat?

​	能够接收并处理http请求，所以tomcat是一个http服务器

![总体架构](.\image\总体架构.png)

http服务器接收到请求之后交给servlet容器来处理，servlet容器通过servlet接口调用业务类

### tomcat的组成

连接器组件coyote和容器组件catalina

连接器：

![连接器组件](.\image\连接器组件.png)

容器组件：

![容器组件](.\image\容器组件.png)

engine:

​	表示整个catalina的servlet引擎，用来管理多个站点

host:

​	代表一个虚拟主机，或者一个站点

context:

​	表示一个web应用程序

wrapper:

​	表示一个servlet