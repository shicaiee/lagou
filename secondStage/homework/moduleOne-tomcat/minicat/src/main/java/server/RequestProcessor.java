package server;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/16 21:55
 **/
public class RequestProcessor extends Thread {

    private Socket accept;

    private Map<String,HttpServlet> servletMap = new HashMap<>();

    public RequestProcessor(Socket accept,Map map){
        this.accept = accept;
        this.servletMap = map;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = accept.getInputStream();

            // 封装对象
            Request request = new Request(inputStream);
            Response response = new Response(accept.getOutputStream());
            //静态资源
            if(servletMap.get(request.getUrl()) == null){
                response.outputHtml(request.getUrl());
            } else {
                // 动态资源
                HttpServlet httpServlet = servletMap.get(request.getUrl());
                httpServlet.service(request,response);
            }
            accept.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
