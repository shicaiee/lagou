package server;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/16 20:25
 **/
public abstract class HttpServlet implements Servlet {
    public abstract void doGet(Request request,Response response);

    public abstract void doPost(Request request,Response response);

    @Override
    public void service(Request request, Response response) throws Exception {
        if("GET".equals(request.getMethod())){
            doGet(request,response);
        }else {
            doPost(request,response);
        }
    }
}
