package server;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * @Desc 封装Response对象需要依赖于OutputStream
 *         该对象需要提供核心方法，输出html
 * @Author Matures
 * @CreateTime 2021/3/15 23:10
 **/
public class Response {

    private OutputStream outputStream;

    public Response(){}

    public Response(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    // 输出指定字符串
    public void output(String content) throws IOException {
        outputStream.write(content.getBytes());
    }

    /**
     * path url,随后要根据url来获取到静态资源的绝对路径，进一步根据路劲读取静态资源文件，最后通过输出流输出
     * @param path
     */
    public void outputHtml(String path) throws IOException {
        // 获取静态资源文件的绝对路径
        String absoluteResourcePath = StaticResourceUtil.getAbsolutePath(path);
        // 输出静态资源文件
        File file = new File(absoluteResourcePath);
        if (file.exists() && file.isFile()) {
            // 读取静态资源文件，输出静态资源
            StaticResourceUtil.outputStaticResource(new FileInputStream(file),outputStream);
        } else {
            // 输出404
            output(HttpProtocolUtil.getHttpHeader404());
        }
    }
}
