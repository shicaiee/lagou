package server;

/**
 * @Desc http协议工具 ,提供响应头信息
 * @Author Matures
 * @CreateTime 2021/3/15 22:48
 **/
public class HttpProtocolUtil {

    /**
     * 为响应码200 提供请求头信息
     * @return
     */
    public static String getHttpHeader200(long contentLength){
        return "HTTP/1.1 200 OK \n"+
                "Content-Type: text/html \n"+
                "Content-Length: "+contentLength+" \n"+
                "\r\n";
    }

    public static String getHttpHeader404(){
        String string = "<h1>404</h1>";
        return "HTTP/1.1 404 NOT Found \n"+
                "Content-Type: text/html \n"+
                "Content-Length: "+string.getBytes().length+" \n"+
                "\r\n" + string;
    }
}
