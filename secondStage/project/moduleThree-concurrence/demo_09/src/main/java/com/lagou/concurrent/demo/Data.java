package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 23:02
 **/
public class Data {

    private float myFloat;
    public void modify(float difference) {
        float value = this.myFloat;
        this.myFloat = value + difference;
        System.out.println(Thread.currentThread().getName()+"--->"+myFloat);
    }

}
