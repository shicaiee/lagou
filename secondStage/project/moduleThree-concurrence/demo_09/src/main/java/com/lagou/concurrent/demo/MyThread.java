package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 17:01
 **/
public class MyThread extends Thread {


    private final Data data;

    public MyThread(Data data){
        this.data = data;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100000; i++) {
            data.modify(1);
        }
    }
}
