package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 10:50
 **/
public class Main {
    public static void main(String[] args) {
        //new MyThread().start();
        new Thread(new MyRunnable()).start();

        System.out.println(Thread.currentThread().getName());
    }
}
