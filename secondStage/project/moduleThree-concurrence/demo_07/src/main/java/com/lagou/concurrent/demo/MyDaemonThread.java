package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 22:07
 **/
public class MyDaemonThread extends Thread {

    @Override
    public void run() {
        while (true){
            System.out.println("daemon 线程运行中");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
