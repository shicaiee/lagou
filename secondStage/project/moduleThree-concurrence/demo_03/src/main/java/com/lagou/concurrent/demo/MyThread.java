package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 10:50
 **/
public class MyThread extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 10 ; i++) {
            System.out.println(getName()+"线程运行起来了"+i);
        }
    }
}
