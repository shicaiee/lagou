package com.lagou.concurrent.demo;

import java.util.concurrent.Callable;

import static java.lang.Thread.sleep;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 11:16
 **/
public class MyCallable implements Callable<String> {


    @Override
    public String call() throws Exception {
        sleep(5000);
        return "MyCallable 的运行返回结果";
    }
}
