package com.lagou.concurrent.demo;

import java.util.concurrent.*;

import static java.lang.Thread.sleep;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 11:20
 **/
public class Main2 {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ThreadPoolExecutor executor = new ThreadPoolExecutor(
                10,10 ,1,
                TimeUnit.SECONDS ,new ArrayBlockingQueue<>(10)){
            @Override
            protected void afterExecute(Runnable r, Throwable t) {
                System.out.println("callable 运行完毕"+t);
            }
        };

        MyCallable myCallable = new MyCallable();
        Future<String> future = executor.submit(myCallable);
        executor.shutdown();
        System.out.println(future.get());
        //sleep(100);
        //System.out.println("main end");
    }

}
