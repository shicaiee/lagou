package com.lagou.concurrent.demo;

import java.util.Random;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 15:31
 **/
public class ProducerThread extends Thread{

    private final MyQueue myQueue;

    private int index = 0;

    private final Random  random = new Random();

    public ProducerThread(MyQueue myQueue){
        this.myQueue = myQueue;
    }

    private final Object lock = new Object();


    @Override
    public void run() {
        while (true){
            String tmp = "线程："+getName()+"生产数据："+index++;
            try {
                myQueue.put(tmp);
                //System.out.println(tmp);
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
