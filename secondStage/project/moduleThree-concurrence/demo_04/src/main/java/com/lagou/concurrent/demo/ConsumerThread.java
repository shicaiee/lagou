package com.lagou.concurrent.demo;

import java.util.Random;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 15:31
 **/
public class ConsumerThread extends Thread{

    private final MyQueue myQueue;

    private int index = 0;

    private final Random  random = new Random();

    public ConsumerThread(MyQueue myQueue){
        this.myQueue = myQueue;
    }


    @Override
    public void run() {
        while (true){
            try {
                myQueue.get();
                //System.out.println("消费数据："+);
                Thread.sleep(random.nextInt(1000));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
