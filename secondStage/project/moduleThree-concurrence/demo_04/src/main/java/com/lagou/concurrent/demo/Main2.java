package com.lagou.concurrent.demo;

import java.util.concurrent.ExecutionException;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 10:50
 **/
public class Main2 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        MyQueue2 myQueue = new MyQueue2();

        for (int i = 0; i < 5; i++) {
            new ProducerThread(myQueue).start();
            new ConsumerThread(myQueue).start();
        }

        Thread.sleep(10000);
        // 进程结束
        System.exit(0);
    }
}
