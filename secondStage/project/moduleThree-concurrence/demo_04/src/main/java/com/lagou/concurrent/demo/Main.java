package com.lagou.concurrent.demo;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 10:50
 **/
public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        MyQueue myQueue = new MyQueue();
        new ConsumerThread(myQueue).start();
        new ConsumerThread(myQueue).start();
        new ProducerThread(myQueue).start();

        Thread.sleep(5000);
        // 进程结束
        System.exit(0);
    }
}
