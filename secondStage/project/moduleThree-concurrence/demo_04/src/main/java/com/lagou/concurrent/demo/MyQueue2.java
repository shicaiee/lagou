package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 15:21
 **/
public class MyQueue2 extends MyQueue{

    // 存放数据
    private String[] data = new String[10];

    private int putIndex = 0;

    private int getIndex = 0;

    private int size;

    @Override
    public synchronized void put(String element){
        if(size == data.length){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            put(element);
        } else{
            put0(element);
        }

    }

    private void put0(String element){
        System.out.println(element);
        data[putIndex] = element;
        notify();
        putIndex++;
        size++;
        if(putIndex == data.length){
            putIndex = 0;
        }
    }

    @Override
    public synchronized String get() throws InterruptedException {
        if(size == 0){
            wait();
            return get();
        } else {
            return get0();
        }
    }

    private String get0(){
        String result = data[getIndex];
        notify();
        getIndex++;
        size--;
        if(getIndex == data.length){
            getIndex = 0;
        }
        System.out.println("消费数据："+result);
        return result;
    }


}
