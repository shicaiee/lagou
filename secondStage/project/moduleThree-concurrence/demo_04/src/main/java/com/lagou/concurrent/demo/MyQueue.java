package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 15:21
 **/
public class MyQueue {

    // 存放数据
    private String[] data = new String[10];

    private int putIndex = 0;

    private int getIndex = 0;

    private int size;

    public synchronized void put(String element){
        if(size == data.length){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        data[putIndex] = element;
        notify();
        putIndex++;
        size++;
        if(putIndex == data.length){
            putIndex = 0;
        }
    }

    public synchronized String get() throws InterruptedException {
        if(size == 0){
            wait();
        }
        String result = data[getIndex];
        notify();
        getIndex++;
        size--;
        if(getIndex == data.length){
            getIndex = 0;
        }
        return result;
    }


}
