package com.lagou.concurrent.demo;

import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/31 22:20
 **/
public class MyThread1 extends Thread {

    private CyclicBarrier barrier;

    private Random random = new Random();

    public MyThread1(String name, CyclicBarrier barrier){
        super(name);
        this.barrier = barrier;
    }

    @Override
    public void run() {

        try {
            sleep(random.nextInt(5000));
            System.out.println(Thread.currentThread().getName()+" - 到达教师");
            barrier.await();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }

    }
}
