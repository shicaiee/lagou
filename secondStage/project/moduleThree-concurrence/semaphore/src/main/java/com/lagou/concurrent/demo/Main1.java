package com.lagou.concurrent.demo;

import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/31 22:20
 **/
public class Main1 {

    public static void main(String[] args) {
        CyclicBarrier barrier = new CyclicBarrier(5, new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName()+" - 阶段任务结束");
            }
        });
        for (int i = 0; i < 5; i++) {
            new MyThread1("学生-"+(i+1),barrier).start();
        }
    }
}
