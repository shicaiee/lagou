package com.lagou.concurrent.demo;

import java.util.concurrent.Semaphore;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/31 22:20
 **/
public class Main {

    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(1);
        for (int i = 0; i < 5; i++) {
            new MyThread("学生-"+(i+1),semaphore).start();
        }
    }
}
