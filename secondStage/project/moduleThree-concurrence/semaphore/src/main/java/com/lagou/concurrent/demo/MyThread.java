package com.lagou.concurrent.demo;

import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/31 22:20
 **/
public class MyThread extends Thread {

    private Semaphore semaphore;

    private Random random = new Random();

    public MyThread(String name,Semaphore semaphore){
        super(name);
        this.semaphore = semaphore;
    }

    @Override
    public void run() {

        try {
            // 获取信标： 类似于抢座位
            semaphore.acquire();
            // 抢到座位 开始写作业
            System.out.println(Thread.currentThread().getName()+"- 抢到座位开始写作业");
            sleep(random.nextInt(1000));
            System.out.println(Thread.currentThread().getName()+"- 作业写完腾出座位");

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        semaphore.release();

    }
}
