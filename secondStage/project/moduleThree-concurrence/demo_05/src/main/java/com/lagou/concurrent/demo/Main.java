package com.lagou.concurrent.demo;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 10:50
 **/
public class Main {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Thread myThread = new MyThread();
        myThread.start();

        Thread.sleep(10);
        // 中断线程  精确含义是“唤醒轻量级阻塞”
        myThread.interrupt();
        Thread.sleep(10);
        System.exit(0);
    }
}
