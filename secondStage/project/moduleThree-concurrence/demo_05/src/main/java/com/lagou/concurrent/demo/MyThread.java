package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 17:01
 **/
public class MyThread extends Thread {

    @Override
    public void run() {
        while (true){
            boolean b = isInterrupted();
            System.out.println("中断状态："+b);
        }
    }
}
