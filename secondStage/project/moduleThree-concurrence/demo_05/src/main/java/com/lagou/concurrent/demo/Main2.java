package com.lagou.concurrent.demo;

import java.util.concurrent.ExecutionException;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 10:50
 **/
public class Main2 {
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Thread myThread = new MyThread2();
        myThread.start();

        Thread.sleep(10);
        // 中断线程
        myThread.interrupt();
        Thread.sleep(10);
        System.exit(0);
    }
}
