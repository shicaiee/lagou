package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 10:50
 **/
public class Main {
    public static void main(String[] args) throws InterruptedException {
        //new MyThread().start();
        Thread thread = new MyThread();
        thread.start();
        thread.join();
        System.out.println(Thread.currentThread().getName()+"运行结束");
    }
}
