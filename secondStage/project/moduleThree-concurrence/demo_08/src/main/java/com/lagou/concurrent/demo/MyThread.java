package com.lagou.concurrent.demo;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/28 17:01
 **/
public class MyThread extends Thread {

    private static boolean flag = true;

    @Override
    public void run() {
        while (flag){
            try {
                Thread.sleep(500);
                System.out.println("非daemon 线程运行中");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // 用于关闭线程
    public void stopRunning(){
        flag = false;
    }

    public static void main(String[] args) throws InterruptedException {
        MyThread myThread = new MyThread();
        myThread.start();
        Thread.sleep(3000);
        myThread.stopRunning();
        myThread.join();
        System.out.println("main stop");
    }
}
