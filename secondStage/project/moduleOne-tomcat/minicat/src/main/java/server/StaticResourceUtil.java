package server;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/15 23:30
 **/
public class StaticResourceUtil {

    /**
     *  获取静态资源的绝对路劲
     * @return
     */
    public static String getAbsolutePath(String path){
        String absolutePath = StaticResourceUtil.class.getClass().getResource("/").getPath();
        return absolutePath.replaceAll("\\\\","/")+path;
    }


    /**
     * 读取静态文件输入流，通过输出流 输出
     */
    public static void outputStaticResource(InputStream inputStream, OutputStream outputStream) throws IOException {
        int count = 0;
        while (count == 0){
            count = inputStream.available();
        }
        int resourceSize = count;
        // http请求头输出，然后再输出巨头内容
        outputStream.write(HttpProtocolUtil.getHttpHeader200(resourceSize).getBytes());
        // 读取内容输出
        long written = 0; // 已经读取的内容长度
        int byteSize = 1024; // 计划缓冲的长度
        byte[] bytes = new byte[byteSize];
        while (written < resourceSize){
            if(written + byteSize > resourceSize){
                byteSize = (int) (resourceSize-written); // 最后剩余的文件长度
                bytes = new byte[byteSize];
            }
            inputStream.read(bytes);
            outputStream.write(bytes);
            outputStream.flush();
            written+=byteSize;
        }
    }
}
