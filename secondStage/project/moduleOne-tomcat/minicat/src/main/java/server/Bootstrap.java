package server;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.*;

/**
 * @Desc Minicat的主类
 * @Author Matures
 * @CreateTime 2021/3/15 22:18
 **/
public class Bootstrap {
    // 定义socket监听的端口号
    private int port = 8080;


    /**
     * Minicat 启动需要初始化展开的一些操作
     */
    public void start() throws Exception {
        // 加载service.xml 文件 读取配置信息
        HashMap context = loadServer();


        // 加载解析相关的配置文件
        loadServlet();

        // 定义一个线程池
        ThreadPoolExecutor threadPoolExecutor =
                new ThreadPoolExecutor(10, 50, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(50),
                        Executors.defaultThreadFactory());


        // 完成minicat 1.0版本 （浏览器请求 http://localhost:8080,返回一个固定的字符串到页面“hello Minicat”）
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Minicat start on：" + port);

        // 1.0
        //while(true){
        //    Socket socket = serverSocket.accept();
        //    // 接收到请求,获取输出流
        //    OutputStream outputStream = socket.getOutputStream();
        //    String response = HttpProtocolUtil.getHttpHeader200("hello Minicat".getBytes().length)+"hello Minicat";
        //    outputStream.write(response.getBytes());
        //    socket.close();
        //}
        /**
         *  完成 Minicat 2.0 版本
         *  需求request 和 response对象，返回html资源文件
         */
        //while (true){
        //    Socket accept = serverSocket.accept();
        //    InputStream inputStream = accept.getInputStream();
        //    // 封装对象
        //    Request request = new Request(inputStream);
        //    Response response = new Response(accept.getOutputStream());
        //    response.outputHtml(request.getUrl());
        //    accept.close();
        //}

        /**
         *  完成 Minicat 3.0 版本
         *  需求 可以请求动态资源
         */
        //while (true){
        //    Socket accept = serverSocket.accept();
        //    InputStream inputStream = accept.getInputStream();
        //    // 封装对象
        //    Request request = new Request(inputStream);
        //    Response response = new Response(accept.getOutputStream());
        //    //静态资源
        //    if(servletMap.get(request.getUrl()) == null){
        //        response.outputHtml(request.getUrl());
        //    } else {
        //        // 动态资源
        //        HttpServlet httpServlet = servletMap.get(request.getUrl());
        //        httpServlet.service(request,response);
        //    }
        //    accept.close();
        //}

        /**
         * 多线程改造（不使用线程池）
         */
        //while (true){
        //    Socket accept = serverSocket.accept();
        //    RequestProcessor requestProcessor = new RequestProcessor(accept, servletMap);
        //    requestProcessor.start();
        //}

        /**
         *  多线程改造（使用线程池）
         */
        //while (true) {
        //    Socket accept = serverSocket.accept();
        //    RequestProcessor requestProcessor = new RequestProcessor(accept, servletMap);
        //    threadPoolExecutor.execute(requestProcessor);
        //}

        while (true) {
            Socket accept = serverSocket.accept();
            RequestProcessorV4 requestProcessor = new RequestProcessorV4(accept, context);
            threadPoolExecutor.execute(requestProcessor);
        }
    }

    private Mapper host = null;

    private HashMap loadServer() {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("service.xml");
        SAXReader saxReader = new SAXReader();
        Document document = null;
        try {
            document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            // connect 信息
            Element element = (Element) rootElement.selectSingleNode("//Connector");
            // 获取监听端口
            this.port = Integer.parseInt(element.attributeValue("port"));

            // Host 信息
            Element host = (Element) rootElement.selectSingleNode("//Host");
            String hostName = host.attributeValue("name");
            String appBasePath = host.attributeValue("appBase");

            // 根据appBasePath 去加载各种demo
            return loadDemo(appBasePath);
        } catch (Exception e) {
        }
        return null;
    }

    // 根据路径记载demo的信息  context
    private HashMap<String, Object> loadDemo(String appBasePath) {
        HashMap<String, Object> context = new HashMap<>();
        File file = new File(appBasePath);
        if (file.exists() && file.isDirectory()) { //存在并且是文件夹
            File[] files = file.listFiles(); // 获取具体的demo文件夹
            for (File child : files) {
                HashMap<String, HttpServlet> map = loadServlet(child.getAbsolutePath());
                context.put(child.getName(), map);
            }
        }
        return context;
    }

    //private HashMap<String, HttpServlet> loadServlet(String path) {
    //    HashMap<String, HttpServlet> servletHashMap = new HashMap<>();
    //    InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream(path);
    //    SAXReader saxReader = new SAXReader();
    //    Document document = null;
    //    try {
    //        document = saxReader.read(resourceAsStream);
    //        Element rootElement = document.getRootElement();
    //        List<Element> elements = rootElement.selectNodes("//servlet");
    //        for (int i = 0; i < elements.size(); i++) {
    //            Element element = elements.get(i);
    //            Element name = (Element) element.selectSingleNode("servlet-name");
    //            String servletName = name.getStringValue();
    //            Element clazz = (Element) element.selectSingleNode("servlet-class");
    //            String servletClass = clazz.getStringValue();
    //
    //            // 根据servlet-name 的值 找到url-pattern
    //            Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
    //            String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
    //            servletHashMap.put(urlPattern, (HttpServlet) Class.forName(servletClass).newInstance());
    //        }
    //    } catch (DocumentException | ClassNotFoundException e) {
    //        e.printStackTrace();
    //    } catch (IllegalAccessException e) {
    //        e.printStackTrace();
    //    } catch (InstantiationException e) {
    //        e.printStackTrace();
    //    }
    //    return servletHashMap;
    //}

    private HashMap<String, HttpServlet> loadServlet(String path) {
        HashMap<String, HttpServlet> servletHashMap = new HashMap<>();
        MyClassLoad myClassLoad = new MyClassLoad(path);
        try {
            InputStream resourceAsStream = new FileInputStream(path+ "\\web.xml");
            SAXReader saxReader = new SAXReader();
            Document document = null;
            document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            List<Element> elements = rootElement.selectNodes("//servlet");
            for (int i = 0; i < elements.size(); i++) {
                Element element = elements.get(i);
                Element name = (Element) element.selectSingleNode("servlet-name");
                String servletName = name.getStringValue();
                Element clazz = (Element) element.selectSingleNode("servlet-class");
                String servletClass = clazz.getStringValue();

                // 根据servlet-name 的值 找到url-pattern
                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
                Class<?> aClass = myClassLoad.loadClass(servletClass);
                servletHashMap.put(urlPattern, (HttpServlet) aClass.newInstance());
            }
        } catch (DocumentException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return servletHashMap;
    }


    private Map<String, HttpServlet> servletMap = new HashMap<>();

    /**
     * 加载解析web.xml 初始化 servlet
     */
    private void loadServlet() {
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("web.xml");
        SAXReader saxReader = new SAXReader();
        Document document = null;
        try {
            document = saxReader.read(resourceAsStream);
            Element rootElement = document.getRootElement();
            List<Element> elements = rootElement.selectNodes("//servlet");
            for (int i = 0; i < elements.size(); i++) {
                Element element = elements.get(i);
                Element name = (Element) element.selectSingleNode("servlet-name");
                String servletName = name.getStringValue();
                Element clazz = (Element) element.selectSingleNode("servlet-class");
                String servletClass = clazz.getStringValue();

                // 根据servlet-name 的值 找到url-pattern
                Element servletMapping = (Element) rootElement.selectSingleNode("/web-app/servlet-mapping[servlet-name='" + servletName + "']");
                String urlPattern = servletMapping.selectSingleNode("url-pattern").getStringValue();
                servletMap.put(urlPattern, (HttpServlet) Class.forName(servletClass).newInstance());
            }
        } catch (DocumentException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    /**
     * Minicat 的程序启动入口
     *
     * @param args
     */
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        try {
            bootstrap.start();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
