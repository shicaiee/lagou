package server;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/16 20:23
 **/
public interface Servlet {

    void init() throws Exception;

    void destory() throws Exception;

    void service(Request request,Response response) throws Exception;
}
