package server;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Desc  把请求信息封装为Rquest对象 （根据InputSteam输入流封装）
 * @Author Matures
 * @CreateTime 2021/3/15 23:10
 **/
public class Request {

    //请求方式
    private String method;

    private String url;

    // 输入流 其他属性从输入流中解析出来
    private InputStream inputStream;

    public Request(){}

    public Request(InputStream inputStream) throws IOException {
        this.inputStream = inputStream;
        // 从输入流中获取请求信息
        int count = 0;
        while (count == 0){
            count = inputStream.available();
        }
        byte[] bytes = new byte[count];
        inputStream.read(bytes);
        String inputString = new String(bytes);
        // 获取第一行请求头信息
        String firstLine = inputString.split("\\n")[0];
        String[] strings = firstLine.split(" ");
        this.method = strings[0];
        this.url = strings[1];
        System.out.println("-->mothod: "+method);
        System.out.println("-->url: "+url);
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }


}
