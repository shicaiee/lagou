package server;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/16 21:55
 **/
public class RequestProcessorV4 extends Thread {

    private Socket accept;

    private Map<String,Object> context = new HashMap<>();

    public RequestProcessorV4(Socket accept, Map map){
        this.accept = accept;
        this.context = map;
    }

    @Override
    public void run() {
        try {
            InputStream inputStream = accept.getInputStream();

            // 封装对象
            Request request = new Request(inputStream);
            Response response = new Response(accept.getOutputStream());
            //静态资源
            String url = request.getUrl(); // localhost:8080/demo1/lagou
            String[] s = url.split("/");
            String context = s[1]; // demo1
            HashMap map = (HashMap<String, HttpServlet>)this.context.get(context);
            HttpServlet servlet = (HttpServlet)map.get("/"+s[2]);
            servlet.service(request,response);
            accept.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
