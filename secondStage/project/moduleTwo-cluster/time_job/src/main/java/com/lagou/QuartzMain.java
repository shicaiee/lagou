package com.lagou;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/25 14:01
 **/
public class QuartzMain {

    /**
     * main函数中开启定时任务
     * @param args
     */
    public static void main(String[] args) throws SchedulerException {
        // 1、创建任务调度器 （好比公交车调度站）
        // 2、创建一个任务 （好比某个公交车的出行）
        // 3、创建任务的事件触发器 （好比这个公交车的出行时间表）
        // 4、使用任务调度器根据时间触发器执行我们的任务
        Scheduler scheduler = QuartzMain.createScheduler();
        JobDetail job = QuartzMain.createJob();
        CronTrigger trigger = QuartzMain.createTrigger();
        scheduler.scheduleJob(job, trigger);
        scheduler.start();

    }

    // 1、创建任务调度器 （好比公交车调度站）
    public static Scheduler createScheduler() throws SchedulerException {
        SchedulerFactory schedulerFactory = new StdSchedulerFactory();
        return schedulerFactory.getScheduler();
    }

    // 2、创建一个任务 （好比某个公交车的出行）
    public static JobDetail createJob(){
        JobBuilder jobBuilder = JobBuilder.newJob(DemoJob.class);
        // todo 自定义任务类
        jobBuilder.withIdentity("jobName","myJob");
        return jobBuilder.build();
    }

    // 3、创建任务的事件触发器 （好比这个公交车的出行时间表）
    public static CronTrigger createTrigger(){
        // 创建时间触发器
        CronTrigger cronTrigger = TriggerBuilder.newTrigger()
                .withIdentity("triggerName","myTrigger")
                .startNow()
                .withSchedule(CronScheduleBuilder.cronSchedule("*/2 * * * * ?")).build();
        return cronTrigger;
    }

}
