package com.lagou.elasticjob;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;

import java.util.List;
import java.util.Map;

/**
 * @Desc elasticJobLite 定时任务业务逻辑处理类
 * @Author Matures
 * @CreateTime 2021/3/25 15:33
 **/
public class ArchivieJob implements SimpleJob {

    /**
     * 需求：resume表中未归档的数据归档到resume_bak表中，
     *      每次归档1条记录
     */
    @Override
    public void execute(ShardingContext shardingContext) {
        int shardingItem = shardingContext.getShardingItem();
        System.out.println("当前分片："+shardingItem);
        // 获取分片参数
        String shardingParameter = shardingContext.getShardingParameter();

        // 1、从resume表中查询一条记录（未归档）
        String selectSql = "select * from resume where state = '未归档' and education = '"+shardingParameter+"' limit 1";
        List<Map<String, Object>> list = JdbcUtil.executeQuery(selectSql);
        if(list == null || list.size() == 0){
            System.out.println("数据处理完毕");
            return;
        }
        // 2、需改状态
        Map<String, Object> stringObjectMap = list.get(0);
        long id = (long) stringObjectMap.get("id");
        String name = (String) stringObjectMap.get("name");
        String state = (String) stringObjectMap.get("state");
        String education = (String) stringObjectMap.get("education");
        System.out.println("id:"+id+"  name:"+name+" state:"+state+" education:"+education);

        String updateSql = "update resume set state = '已归档' where id = ?";
        JdbcUtil.executeUpdate(updateSql,id);

        // 3、归档这条记录，插入到resume_bak表
        String insertSql = "insert into resume_bak select * from resume where id = ?";
        JdbcUtil.executeUpdate(insertSql,id);
    }
}
