package com.lagou.elasticjob;

import com.dangdang.ddframe.job.config.JobCoreConfiguration;
import com.dangdang.ddframe.job.config.simple.SimpleJobConfiguration;
import com.dangdang.ddframe.job.lite.api.JobScheduler;
import com.dangdang.ddframe.job.lite.config.LiteJobConfiguration;
import com.dangdang.ddframe.job.reg.base.CoordinatorRegistryCenter;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/25 15:54
 **/
public class ElasticJobMain {

    public static void main(String[] args) {
        // 流程和Quartz流程相似
        // 配置分布式协调服务（注册中心） Zookeeper
        ZookeeperConfiguration configuration =
                new ZookeeperConfiguration("localhost:2181", "data-archive-job");
        CoordinatorRegistryCenter registryCenter = new ZookeeperRegistryCenter(configuration);
        registryCenter.init();
        // 配置任务 时间、任务、调度器
        JobCoreConfiguration jobCoreConfiguration = JobCoreConfiguration.newBuilder("archive-job", "*/15 * * * * ?", 3)
                .shardingItemParameters("0=bachelor,1=master,2=doctor").build();
        SimpleJobConfiguration simpleJobConfiguration = new SimpleJobConfiguration(jobCoreConfiguration,ArchivieJob.class.getName());
        new JobScheduler(registryCenter, LiteJobConfiguration.newBuilder(simpleJobConfiguration)
                .overwrite(true).build()).init();
    }

}
