/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/23 22:59
 **/
public class GeneralHash {
    public static void main(String[] args) {
        // 定义客户端ip
        String[] clients = new String[]{"10.78.12.3","101.78.131.13","21.18.12.3"};

        // 定义服务器数量
        int serverCount = 9;

        // 路由计算
        for(String client: clients){
            // hash(ip)%node_counts = index
            int hash = Math.abs(client.hashCode());
            int index = hash%serverCount;
            System.out.println("客户端："+client+" 被路由到的服务器编号为："+index);
        }
    }
}
