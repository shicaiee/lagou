import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/3/23 23:29
 **/
public class ConsistentHashWithVirtual {
    public static void main(String[] args) {
        // 初始化： 把服务器节点ip的哈希值对应到哈希环上
        // 定义服务器ip
        String[] tomcatServers = new String[]{"10.78.12.0","101.78.131.1","21.18.12.2","81.48.12.3"};
        SortedMap<Integer,String> hashServerMap = new TreeMap<>();

        // 定义针对每个真是服务器虚拟出来几个节点
        int virtualCount = 3;

        for (String tomcatServer : tomcatServers) {
            // 求出每一个ip的哈希值，对应到hash环上，存储hash值与ip的对应关系
            int serverHash = Math.abs(tomcatServer.hashCode());
            // 存储hash值与ip的对应关系
            hashServerMap.put(serverHash,tomcatServer);
            // 处理虚拟节点
            for (int i = 0; i < virtualCount ; i++) {
                int virtualHash = Math.abs((tomcatServer+"#"+i).hashCode());
                hashServerMap.put(virtualHash,"---由虚拟节点"+i+"映射过来的请求"+tomcatServer);
            }
        }

        // 第二步：针对客户端IP求出hash值
        // 定义客户端ip
        String[] clients = new String[]{"10.78.12.3","101.78.131.13","21.18.12.3"};
        for (String client : clients) {
            int clientHash = Math.abs(client.hashCode());
            // 第三步：针对客户端找到能够找到处理当前客户端请求的服务器（哈希环上顺时针最近）
            // 根据客户端ip的哈希值，找出哪一个服务器节点能够处理
            SortedMap<Integer, String> integerStringSortedMap = hashServerMap.tailMap(clientHash);
            if(integerStringSortedMap.isEmpty()){
                // 取hash环上的第一台服务器，顺时针
                Integer firstKey = hashServerMap.firstKey();
                System.out.println("=========>>>客户端："+client+"被路由到服务器："+hashServerMap.get(firstKey));
            }else {
                System.out.println("=========>>>客户端："+client+"被路由到服务器："+hashServerMap.get(integerStringSortedMap.firstKey()));
            }
        }
    }

}
