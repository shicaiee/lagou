package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class Producer {

    private static final String[] LOG_LEVEL = {"info","error","warn"};

    private static final String[] LOG_AREA = {"beijing","shanghai","shenzhen"};

    private static final String[] LOG_BIZ = {"edu-online","biz-online","emp-online"};

    private static final Random RANDOM = new Random();

    public static void main(String[] args) throws Exception{
        // 创建连接工厂
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("ex.topic", BuiltinExchangeType.TOPIC);

        // 定义发送消息
        String message, routingKey;
        String area, biz, level;

        for (int i = 0; i < 30; i++) {
            // 对具体消息参数赋值
            area = LOG_AREA[RANDOM.nextInt(LOG_AREA.length)];
            biz = LOG_BIZ[RANDOM.nextInt(LOG_BIZ.length)];
            level = LOG_LEVEL[RANDOM.nextInt(LOG_LEVEL.length)];
            // 定义路由键
            routingKey = area + "." + biz + "." + level;
            message = "LOG: [" + level + "]: 这是 [" + area + "] 地区 ["
                    + biz + "] 服务器发来的消息，MSG_SEQ = " + i;
            // 通道发送消息
            channel.basicPublish("ex.topic",routingKey,null,
                    message.getBytes(StandardCharsets.UTF_8));

        }
        channel.close();
        connection.close();
    }

}
