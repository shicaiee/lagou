package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class ErrorConsumer {

    public static void main(String[] args) throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        // 临时队列，返回值
        String queue = channel.queueDeclare().getQueue();
        channel.queueBind(queue,"ex.topic","#.error.#");

        channel.basicConsume(queue,(consumeTag,message)->{
            System.out.println(new String(message.getBody(),"utf-8"));
        },consumerTag ->{});


    }
}
