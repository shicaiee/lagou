package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 15:25
 **/
public class ErrorConsumer {

    public static void main(String[] args) throws Exception {
        // 创建工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 创建连接对象
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = connectionFactory.newConnection();
        // 创建信道
        final Channel channel = connection.createChannel();

        //channel.exchangeDeclare("ex.router", BuiltinExchangeType.DIRECT);
        // 声明通道
        channel.queueDeclare("error.queue",true,false,false,null);
        // 绑定交换器
        channel.queueBind("error.queue","ex.router","ERROR");

        channel.basicConsume("error.queue",(consumerTag, message) -> {
            System.out.println("ERROR  " + new String(message.getBody()));
            channel.basicAck(message.getEnvelope().getDeliveryTag(),true);
        },consumerTag -> {});

    }
}
