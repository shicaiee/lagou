package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.*;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 15:25
 **/
public class InfoConsumer {

    public static void main(String[] args) throws Exception {
        // 创建工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 创建连接对象
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = connectionFactory.newConnection();
        // 创建信道
        final Channel channel = connection.createChannel();

        channel.exchangeDeclare("ex.router", BuiltinExchangeType.DIRECT);
        // 声明通道
        channel.queueDeclare("info.queue",true,false,false,null);
        // 绑定交换器
        channel.queueBind("info.queue","ex.router","INFO");

        channel.basicConsume("info.queue",(consumerTag, message) -> {
            System.out.println("INFO  " + new String(message.getBody()));
            channel.basicAck(message.getEnvelope().getDeliveryTag(),true);
        },consumerTag -> {});


    }
}
