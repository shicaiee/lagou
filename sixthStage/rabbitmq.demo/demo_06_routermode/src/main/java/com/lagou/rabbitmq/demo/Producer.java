package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import javax.print.DocFlavor;
import java.util.Random;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 19:47
 **/
public class Producer {

    private static String[] LOG_LEVEL = new String[]{"ERROR","INFO"};

    public static void main(String[] args) throws Exception {
        // 创建工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 创建连接对象
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = connectionFactory.newConnection();
        // 创建信道
        final Channel channel = connection.createChannel();



        for (int i = 0; i < 20; i++) {
            String level = LOG_LEVEL[new Random().nextInt(100) % LOG_LEVEL.length];
            channel.basicPublish("ex.router",level,null,("this is " + level).getBytes("utf-8"));
        }

        channel.close();
        connection.close();
    }
}
