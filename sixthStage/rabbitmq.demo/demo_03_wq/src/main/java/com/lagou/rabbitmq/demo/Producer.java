package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 12:21
 **/
public class Producer {

    public static void main(String[] args) throws Exception{
        // 创建工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 创建连接对象
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = connectionFactory.newConnection();
        // 创建信道
        final Channel channel = connection.createChannel();
        // 申明一个消息队列
        channel.queueDeclare("queue.wq",true,false,false,null);
        // 申明一个交换器
        channel.exchangeDeclare("ex.wq", BuiltinExchangeType.DIRECT,true,false,null);
        // 将消息队列绑定到指定的交换器，并指定绑定键
        channel.queueBind("queue.wq","ex.wq","key.wq");

        for (int i = 0; i < 15; i++) {
            channel.basicPublish("ex.wq","key.wq",null,("工作队列：" + i).getBytes("utf-8"));

        }

        channel.close();
        connection.close();



    }

}
