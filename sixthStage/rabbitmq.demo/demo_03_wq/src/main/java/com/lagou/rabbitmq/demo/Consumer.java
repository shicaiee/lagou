package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 12:30
 **/
public class Consumer {

    public static void main(String[] args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();
        channel.queueDeclare("queue.wq",true,false,false,null);

        channel.basicConsume("queue.wq", new DeliverCallback() {
            @Override
            public void handle(String consumerTag, Delivery message) throws IOException {
                System.out.println("推送来的消息：" + new String(message.getBody(),"utf-8"));
                channel.basicAck(message.getEnvelope().getDeliveryTag(),true);
            }
        }, new CancelCallback() {
            @Override
            public void handle(String consumerTag) throws IOException {
                System.out.println("Cancel: " + consumerTag);
            }
        });

    }


}
