package com.lagou.rabbitmq.demo;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.MessagePropertiesBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.nio.charset.StandardCharsets;

public class ProducerApp {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring-rabbit.xml");
        RabbitTemplate rabbitTemplate = context.getBean(RabbitTemplate.class);
        // 构建rabbitmq的message 设置编码和contentType
        Message message;
        MessagePropertiesBuilder builder = MessagePropertiesBuilder.newInstance();
        builder.setContentEncoding("utf-8");
        builder.setContentType(MessageProperties.CONTENT_TYPE_TEXT_PLAIN);

        message = MessageBuilder.withBody("hello world".getBytes(StandardCharsets.UTF_8)).andProperties(builder.build()).build();

        rabbitTemplate.send("ex.direct","routing.q1",message);
        context.close();
    }


}
