package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 15:25
 **/
public class Producer {

    public static void main(String[] args) throws Exception {
        // 创建工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 创建连接对象
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = connectionFactory.newConnection();
        // 创建信道
        final Channel channel = connection.createChannel();
        // 申明fanout类型的交换器
        channel.exchangeDeclare("ex.myfan","fanout",true,false,null);

        for (int i = 0; i < 20 ; i++) {
            channel.basicPublish("myfan",
                    // fanout 类型的交换器不需要指定路由键
                    "",
                    null,("hello world fan:" + i).getBytes("utf-8"));
        }

        channel.close();
        connection.close();

    }
}
