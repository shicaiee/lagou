package com.lagou;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class HelloProducer {

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("47.99.176.18");
        connectionFactory.setVirtualHost("/");
        connectionFactory.setUsername("root");
        connectionFactory.setPassword("123456");
        connectionFactory.setPort(5672);
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();

        /**
         * 消息队列名称
         * 是否是持久化的
         * 是否是排他的
         * 是否是自动删除的
         * 消息队列属性信息
         */
        channel.queueDeclare("queue.biz",false,false,true,null);
        /**
         * 交换器名称
         * 交换器类型
         * 是否是持久化的
         * 是否是自动删除的
         * 交换器的属性集合
         */
        channel.exchangeDeclare("ex.biz", BuiltinExchangeType.DIRECT,false,false,null);

        channel.queueBind("queue.biz","ex.biz","hello.world");

        /**
         * 交换器的名称
         * 消息的路由键
         * 消息的属性MessageProperties对象
         * 消息的字节数组
         */
        channel.basicPublish("ex.biz","hello.world",null,"hello world 2 ".getBytes());

        channel.close();

        connection.close();
    }

}
