package com.lagou;

import com.rabbitmq.client.*;

import java.io.IOException;

public class HelloConsumeConsumer {

    public static void main(String[] args) throws Exception {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        /**
         * 指定协议：amqp://
         * 指定用户名：root
         * 指定密码：123456
         * 指定host：
         * 指定端口号
         * 指定虚拟机 %2f
         */
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare("queue.biz",false,false,true,null);

        channel.basicConsume("queue.biz", (consumerTag, message) -> {
            System.out.println(new String(message.getBody()));
            channel.basicAck(message.getEnvelope().getDeliveryTag(),true);
        },(consumerTag)->{});

//        channel.close();
//        connection.close();
    }
}
