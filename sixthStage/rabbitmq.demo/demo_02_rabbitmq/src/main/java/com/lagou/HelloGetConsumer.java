package com.lagou;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.GetResponse;

public class HelloGetConsumer {

    public static void main(String[] args) throws Exception{
        ConnectionFactory connectionFactory = new ConnectionFactory();
        /**
         * 指定协议：amqp://
         * 指定用户名：root
         * 指定密码：123456
         * 指定host：
         * 指定端口号
         * 指定虚拟机 %2f
         */
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        Connection connection = connectionFactory.newConnection();
        Channel channel = connection.createChannel();

        GetResponse response = channel.basicGet("queue.biz", true);
        final byte[] body = response.getBody();
        System.out.println(new java.lang.String(body));
        channel.close();
        connection.close();
    }

}
