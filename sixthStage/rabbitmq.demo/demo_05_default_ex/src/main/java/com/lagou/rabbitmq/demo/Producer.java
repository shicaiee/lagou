package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 16:07
 **/
public class Producer {

    public static void main(String[] args) throws Exception {
        // 创建工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 创建连接对象
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = connectionFactory.newConnection();
        // 创建信道
        final Channel channel = connection.createChannel();


        channel.queueDeclare("default.ex.queue",true,false,false,null);
        channel.basicPublish("","default.ex.queue",null,"hello world".getBytes("utf-8"));

        channel.close();
        connection.close();
    }
}
