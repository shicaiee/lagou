package com.lagou.rabbitmq.demo;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.GetResponse;

/**
 * @Desc
 * @Author Matures
 * @CreateTime 2021/12/19 15:25
 **/
public class Consumer {

    public static void main(String[] args) throws Exception {
        // 创建工厂对象
        ConnectionFactory connectionFactory = new ConnectionFactory();
        // 创建连接对象
        connectionFactory.setUri("amqp://root:123456@47.99.176.18:5672/%2f");
        final Connection connection = connectionFactory.newConnection();
        // 创建信道
        final Channel channel = connection.createChannel();

        GetResponse getResponse = channel.basicGet("default.ex.queue", true);
        System.out.println("收到消息：" + new String(getResponse.getBody(),"utf-8"));
    }
}
