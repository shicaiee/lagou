package com.lagou.demo;

public class KouZhao {

    private int id;

    private String type;

    public KouZhao(int id, String type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String toString() {
        return "KouZhao{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
