package com.lagou.demo;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    /**
     * 消费口罩
     */

    private BlockingQueue<KouZhao> queue;

    public Consumer(BlockingQueue<KouZhao> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {

        while (true){

            try {
                Thread.sleep(1000);
                KouZhao take = queue.take();
                System.out.println("消费了一个口罩：" + take.toString());
                System.out.println("仓库剩余口罩数量：" + queue.size());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }

    }
}
