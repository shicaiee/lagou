package com.lagou.demo;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    /**
     * 生产口罩
     */

    private BlockingQueue<KouZhao> queue;

    private Integer index = 0;

    public Producer(BlockingQueue<KouZhao> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        while(true){
            try {
                Thread.sleep(500);
                if (queue.remainingCapacity()<1) {
                    KouZhao n95 = new KouZhao(index++, "N95");
                    queue.put(n95);
                    System.out.println("生产了一个口罩，编号：" + n95.getId());
                } else System.out.println("仓库已满暂停生产");


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
