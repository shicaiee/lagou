package com.lagou.demo;

import java.util.concurrent.ArrayBlockingQueue;

public class Application {

    public static void main(String[] args) {
        ArrayBlockingQueue<KouZhao> kouZhaos = new ArrayBlockingQueue<>(20);
        new Thread(new Producer(kouZhaos)).start();
        new Thread(new Consumer(kouZhaos)).start();
    }
}
