package com.lagou;

import javafx.application.Application;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JedisClusterApplication {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
