//package com.lagou.config;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import redis.clients.jedis.HostAndPort;
//import redis.clients.jedis.JedisCluster;
//
//import java.util.HashSet;
//
//@Configuration
//public class RedisConfig {
//
//    @Value("${spring.redis.cluster.nodes}")
//    private String nodes;
//
//    @Bean
//    public JedisCluster getJedisCluster(){
//        // 获取redis集群的ip 及端口等相关信息
//        String[] serverArray = nodes.split(",");
//        HashSet<HostAndPort> nodes = new HashSet<>();
//        for (String ipPort : serverArray) {
//            String[] ipPortPair = ipPort.split(":");
//            nodes.add(new HostAndPort(ipPortPair[0].trim(),Integer.valueOf(ipPortPair[1].trim())));
//        }
//        return new JedisCluster(nodes,5000);
//    }
//
//
//}
