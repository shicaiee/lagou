import com.lagou.JedisClusterApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = JedisClusterApplication.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class JedisClusterTest {

    private static JedisCluster jedis;
    static {
        // 添加集群的服务节点Set集合
        Set<HostAndPort> hostAndPortsSet = new HashSet<HostAndPort>();
        // 添加节点
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7001));
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7002));
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7003));
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7004));
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7005));
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7006));
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7007));
        hostAndPortsSet.add(new HostAndPort("47.99.176.18", 7008));

        // Jedis连接池配置
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        // 最大空闲连接数, 默认8个
        jedisPoolConfig.setMaxIdle(100);
        // 最大连接数, 默认8个
        jedisPoolConfig.setMaxTotal(500);
        //最小空闲连接数, 默认0
        jedisPoolConfig.setMinIdle(0);
        // 获取连接时的最大等待毫秒数(如果设置为阻塞时BlockWhenExhausted),如果超时就抛异常, 小于零:阻塞不确定的时间,  默认-1
        jedisPoolConfig.setMaxWaitMillis(2000); // 设置2秒
        //对拿到的connection进行validateObject校验
        jedisPoolConfig.setTestOnBorrow(true);
        jedis = new JedisCluster(hostAndPortsSet, jedisPoolConfig);
    }

    /**
     * 测试key:value数据
     * 集群中flushDB、keys废弃
     */
    @Test
    public void testKey() throws InterruptedException {
        jedis.set("name1","1");
        jedis.set("name2","1");
        jedis.set("name3","1");
        jedis.set("name4","1");
        jedis.set("name5","1");
        jedis.set("name6","1");
        jedis.set("name7","1");
        jedis.set("name8","1");
    }
    @Test
    public void test2() throws InterruptedException {
        jedis.set("name7","1");
        System.out.println(jedis.get("name7"));
    }


}
