package com.matures.dao.impl;

import com.matures.bean.Resume;
import com.matures.dao.ResumeDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository("resumeDao")
public class ResumeDAOImpl implements ResumeDAO {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void insertResume(Resume resume) {
        mongoTemplate.insert(resume,"lg_resume");
    }
}
