package com.matures;

import com.matures.bean.Resume;
import com.matures.dao.ResumeDAO;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MongoTemplateTest {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");


        ResumeDAO resumeDao = context.getBean("resumeDao", ResumeDAO.class);

        Resume resume = new Resume();
        resume.setName("zhangsan");
        resume.setCity("beijing");
        String dateStr = "yyyy-MM-dd hh:mm:ss";
        SimpleDateFormat dateFormat = new SimpleDateFormat(dateStr);
        Date parse = null;
        try {
             parse = dateFormat.parse("2020-10-01 11:13:14");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        resume.setBirthday(parse);
        resume.setExpectSalary(50000);
        resumeDao.insertResume(resume);
    }

}
