package com.matures;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class TestMongo {

    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("10.211.55.3", 27017);

        MongoDatabase lg_resume = mongoClient.getDatabase("lg_resume");

        MongoCollection<Document> collection = lg_resume.getCollection("lg_resume_preview");

        Document document = Document.parse("{name:'lisi',city:'nanjing',salary:30000,birthday:new ISODate('2020-8-1')} ");

        collection.insertOne(document);

        mongoClient.close();
    }


}
