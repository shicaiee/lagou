package com.matures;

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class DocumentFindTest {

    public static void main(String[] args) {
        MongoClient mongoClient = new MongoClient("10.211.55.3", 27017);

        MongoDatabase lg_resume = mongoClient.getDatabase("lg_resume");

        MongoCollection<Document> collection = lg_resume.getCollection("lg_resume_preview");

        FindIterable<Document> documents = collection.find();

        for (Document document : documents) {
            System.out.println(document);
        }

        mongoClient.close();
    }


}
