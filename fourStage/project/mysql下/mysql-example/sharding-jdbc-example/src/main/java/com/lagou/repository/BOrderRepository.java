package com.lagou.repository;

import com.lagou.entity.BOrder;
import com.lagou.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BOrderRepository extends JpaRepository<BOrder,Long> {


}
