package dao;

import com.lagou.RunBoot;
import com.lagou.entity.CUser;
import com.lagou.entity.Position;
import com.lagou.entity.PositionDetail;
import com.lagou.repository.CUserRepository;
import com.lagou.repository.PositionDetailRepository;
import com.lagou.repository.PositionRepository;
import org.apache.shardingsphere.transaction.annotation.ShardingTransactionType;
import org.apache.shardingsphere.transaction.core.TransactionType;
import org.apache.shardingsphere.transaction.core.TransactionTypeHolder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunBoot.class)
public class TestShardingTransaction {

    @Resource
    private CUserRepository userRepository;

    @Resource
    private PositionRepository positionRepository;

    @Resource
    private PositionDetailRepository positionDetailRepository;


    @Test
    @Transactional
//    @ShardingTransactionType(TransactionType.XA)
    public void test1(){
        TransactionTypeHolder.set(TransactionType.XA);
        for (int i = 1; i <= 20; i++) {
            Position position = new Position();
            position.setName("lagou" + i);
            position.setSalary("10000000");
            position.setCity("beijing");
            positionRepository.save(position);
            if(i == 3){
                throw new RuntimeException("error");
            }
            PositionDetail positionDetail = new PositionDetail();
            positionDetail.setPid(position.getId());
            positionDetail.setDescription("this is a message " + i);
            positionDetailRepository.save(positionDetail);
        }
    }

    @Test
    public void test2(){
        List<CUser> list = userRepository.findByPwd("ABC");
        System.out.println(list);
    }

}
