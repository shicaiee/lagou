package dao;

import com.lagou.RunBoot;
import com.lagou.entity.CUser;
import com.lagou.entity.City;
import com.lagou.repository.CUserRepository;
import com.lagou.repository.CityRepository;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunBoot.class)
public class TestEncryptor {

    @Resource
    private CUserRepository userRepository;


    @Test
    public void test1(){
        CUser user = new CUser();
        user.setName("lagou1");
        user.setPwd("ABC");
        userRepository.save(user);
    }

    @Test
    public void test2(){
        List<CUser> list = userRepository.findByPwd("ABC");
        System.out.println(list);
    }

}
