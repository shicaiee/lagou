package dao;

import com.lagou.RunBoot;
import com.lagou.entity.*;
import com.lagou.repository.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Random;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunBoot.class)
public class TestShardingMasterSlave {


    @Resource
    private COrderRepository orderRepository;


    @Test
//    @Repeat(100)
    public void testShardingCOrder(){
        for (int i = 0; i < 30; i++) {
            Random random = new Random();
            int userId = random.nextInt(10);
            COrder order = new COrder();
            order.setDel(false);
            order.setCompanyId(1231);
            order.setPositionId(12312);
            order.setUserId(userId);
            order.setPositionId(123123);
            order.setResumeType(1);
            order.setStatus("AUTO");
            order.setCreateTime(new Date());
            order.setUpdateTime(new Date());
            order.setPublishUserId(242342);
            orderRepository.save(order);
        }

    }

    @Test
//    @Repeat(100)
    public void testShardingCOrder1(){
        List<COrder> orders = orderRepository.findAll();
        System.out.println(orders);
    }

}
