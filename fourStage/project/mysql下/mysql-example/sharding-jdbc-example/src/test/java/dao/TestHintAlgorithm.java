package dao;

import com.lagou.RunBoot;
import com.lagou.entity.City;
import com.lagou.repository.CityRepository;
import org.apache.shardingsphere.api.hint.HintManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = RunBoot.class)
public class TestHintAlgorithm {

    @Resource
    private CityRepository cityRepository;


    @Test
    public void test1(){
        HintManager instance = HintManager.getInstance();
        instance.setDatabaseShardingValue(1l);
        List<City> list = cityRepository.findAll();
        list.forEach(city -> {
            System.out.println(city);
        });
    }

}
