package com.lagou;

import com.lagou.bean.Resume;
import com.lagou.dao.ResumeDAO;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class MongoTemplateMain {
    public static void main(String[] args) {
        /* ClassPathXmlApplicationContext applicationContext =
                new ClassPathXmlApplicationContext("applicationContext.xml"); */
        ApplicationContext  applicationContext  = SpringApplication.run(MongoTemplateMain.class,args);
        ResumeDAO  resumeDao = applicationContext.getBean("resumeDao",ResumeDAO.class);
        for (int i = 0; i < 100; i++) {
            Resume  resume  = new Resume();
            resume.setName("lisi"+i);
            resume.setCity("北京");
            Date date = null;
            String  dateStr = "yyyy-MM-dd hh:mm:ss";
            SimpleDateFormat  simpleDateFormat  = new SimpleDateFormat(dateStr);
            try {
                date = simpleDateFormat.parse("2003-11-02 11:13:14");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            resume.setBirthday(date);
            resume.setExpectSalary(28000);
            resumeDao.insertResume(resume);
        }


//        System.out.println("resume="+resume);
//
//        Resume  resume2  =resumeDao.findByName("lisi");
//        System.out.println(resume2);
//        List<Resume> datas = resumeDao.findList("zhangsan");
//        System.out.println(datas);
//        List<Resume> datas2 = resumeDao.findListByNameAndExpectSalary("zhangsan",25000);
//        System.out.println(datas2);
    }
}
